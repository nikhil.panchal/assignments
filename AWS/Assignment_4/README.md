___________________________________
# ASSIGNMENT 4 : AWS
____________________________________
````
DAY 4

What you will learn from this lab

EC2 instance
Launch Template
Autoscaling group (ASG)
Load balancer(LB)
Userdata

-----------------
Task

Create EC2 in private subnet.
Setup Application in your EC2 instance (nginx with welcome page {welcome your_name in ninja batch   X}) .
Create AMI of that EC2 Instance.
Create Launch Template with (version)v1.
Create Autoscaling group(thresold value should be 60% of compute resources).
Create lb and attach it to ASG.
Hit your Loadbalancer's dns arn and see if the ec2 instance page is coming or not.
Now update the previous Launch Template using userdata script changing its welcome page {welcome your_buddy_name in ninja batch   X}.
Now increase your load using stress on ec2.
Now  check if the number of ec2's has increased according to your policy.
Now kill the stress process to make average load below 60%.
Now  check if the number of ec2's has decreased according to your policy.


Instructions to perform this lab

Perform all the steps
Once you have performed all the steps  then make a Readme Doc with step by step representation of all the things you have done and attach screenshot in each step with explaination.
````

# TASK

## task:1 Create EC2 in private subnet.

step:1 -> create VPC.

Step:2 -> create subnet.

Step:3 -> create NAT Gateway for private subnet.

Step:4 -> create Route Table.

Step:5 -> connect subnet and NAT Gateway to create private subnet.

Step:6 -> Go to EC2 service.

Step:7 -> click on launch instance.

Step:8 -> select VPC which is created and select private subnet.

step:9 -> private EC2 is created.

**RESULT**

![](photo/private_ec2.png)

## task:2 Setup Application in your EC2 instance (nginx with welcome page {welcome your_name in ninja batch   X} ).

step:1 -> SSH to public EC2
![](photo/ssh_pub.png)


Step:2 -> then SSH to Private EC2
![](photo/ssh_pri.png)

Step:3 -> update software
```
$ sudo apt-get update
```
![](photo/apt_update.png)


Step:4 -> install nginx
````
$ sudo apt-get install -y nginx
````
![](photo/nginx-install.png)


Step:5 -> start nginx service
````
$ sudo systemctl start nginx
````
check nginx is working or not ?
````
$ curl localhost
````
![](photo/start-nginx.png)

Step:6 -> no go to /var/www/html and vim index.nginx-debian.html and edit content to " welcome NIKHIL in ninja batch "
````
$ sudo vim /var/www/html/index.nginx-debian.html
````
![](photo/index-edit.png)

Step:7 -> curl localhost and observe changes.
````
$ curl localhost
````
![](photo/curl-localhost.png)

so home page of nginx is changed.

## task:3 Create AMI of that EC2 Instance.

step:1 -> Go to instance -> select instance -> action tab -> image and templates -> create image.

![](photo/IMAGE-create.png)


Step:2 -> Give image name and add description which can help you usage of this image in future.
![](photo/image-step2.png)

Step:3 -> go to bottom of the page and click on create image.
"image is created successfully"
![](photo/image-done.png)


## task:3 Create Launch Template with (version)v1.

step:1 -> Go to launch Template -> click on create launch Template.

![](photo/launch_tempelet.png)


Step:2 -> Give image name and add description which can help you usage of this image in future.
![](photo/create-template.png)

Step:3 -> click on create Template.
"Template is created successfully"
![](photo/template-done.png)


## task:4 Create Autoscaling group(thresold value should be 60% of compute resources).

step:1 Go to autoscaling group -> click on create autoscaling group.

![](photo/ASG.png)

Step:2 -> add name & select template -> click on next
![](photo/ASG-step1.png)

Step:3 -> Choose instance launch options -> click on next
![](photo/ASG-step2.png)

Step:4 -> load balancer : create new and attach it.
![](photo/ASG-step3.png)

Step:5 -> Configure group size and scaling policies
![](photo/ASG-step4.png)

Step:6 -> Add notifications
![](photo/ASG-step5.png)

Step:7 -> Add tags
![](photo/ASG-step6png.png)

Step:8 -> Review -> click on create ASG
![](photo/ASG-step7.png)



## task:5 Hit your Loadbalancer's dns arn and see if the ec2 instance page is coming or not.

![](photo/DNS1.png)

## task:6 Now update the previous Launch Template using userdata script changing its welcome page {welcome your_buddy_name in ninja batch   X}.

![](photo/DNS2.png)

## task:7 Now increase your load using stress on ec2.

1. we will install stress so that we can increase stress on private ec2.
```
$ sudo apt-get install stress
```
![](photo/stress_install.png)

2. now we will increase stress.
![](photo/stress_increas.png)

***RESULT**
1. Now  check if the number of ec2's has increased according to your policy.

as we can see here new instance is created.
![](photo/instance_created.png)

2. Now kill the stress process to make average load below 60%.
here i just kill stress command
![](photo/kill_stress.png)


3. Now  check if the number of ec2's has decreased according to your policy.

so instance got terminated.
![](photo/treminated.png)



## AUTHOR
[NIKHIL.PANCHAL]


