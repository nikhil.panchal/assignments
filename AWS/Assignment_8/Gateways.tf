resource "aws_internet_gateway" "MySQL_IG" {
  vpc_id = aws_vpc.main_vpc.id
     tags = {
    "Name" = var.IG_NAME
  }
}

resource "aws_eip" "My_SQL_EPI" {
  vpc = true
}

resource "aws_nat_gateway" "MySQL_NAT_IG" {
  allocation_id = aws_eip.My_SQL_EPI.id
  subnet_id = aws_subnet.Public.id

  tags = {
    "Name" = "MySQL_NAT_IG"
  }
}