# ASSIGNMENT 8 : AWS
_________________________________

`````
@here Good Morning Ninja`s Please find the assignment below:
## Infrastructure as a Code
### Task Description
- Create the following resources using terraform:
    - Create 1 VPC
        - eg : `ninja-vpc-01`
    - Create 4 Subnet
        - 2 public subnet
            - eg : `ninja-pub-sub-01/02`
        - 2 private subnet
            - eg : `ninja-priv-sub-01/02`
    - Create 1 IGW
        - eg : `ninja-igw-01`
    - Create 1 NAT
        - eg : `ninja-nat-01`
    - Create 2 Route Table
        - 1 for public subnet
            - eg : `ninja-route-pub-01/02`
        - 1 for private subnet
            - eg : `ninja-route-priv-01/02`
    - Create  ec2-instances based on your assigned software(cluster or HA)
        - eg : `mongo-master`
        - eg : `mongo-slave-1`
    - Create one bastion host
        - eg : `bastion-host`
    - Create  Security Groups for  your EC2 instances
        - port 22 of bastion host should only accessible from your public ip.
        - port 22 of the private instances should only be accessible from  bastion host
        - port {Software_Port} of the private instances should only be accessible from bastion host
**Note: Make maximum use of  variables and output files**
`````
## Task 
## After Creating all modules under the respective directory, it's the tree structure looks like below

![](photo/1.png)

## Execute terraform init commands inside your workspace
- The terraform init command is used to initialize a working directory containing terraform files.

![](photo/2.png)

## Then execute terraform plan command after the initializing the working directory
- The terraform plan command creates an execution plan, which lets you preview the changes that Terraform plans to make to your infrastructure. 

![](photo/4.png)

### 1. VPC

![](photo/(4)1_vpc.png)

### 2. Subnets

![](photo/(4)1a_subnet.png)
![](photo/(4)1b_subnet.png)
![](photo/(4)1c_subnet.png)
![](photo/(4)1d_subnet.png)

### 3. NAT Gateway

![](photo/(5)0_eip.png)
![](photo/(5)terra_nat_gateway.png)

### 4. Internet Gateway

![](photo/(6)terra_igw.png)


### 5. Route Table

### ``` public subnet route tables```
![](photo/(7)1_rt_pub.png)
![](photo/(7)2_rt_pub_associaltion.png)

### ``` private subnet route tables```
![](photo/(7)3_rt_priv.png)
![](photo/(7)4_rt_priv_association.png)

### 6. Instances

![](photo/(8)1_bastion_host.png)
![](photo/(8)2_mongo_master.png)
![](photo/(8)2_mongo_slave-1.png)

### 7. Security group

![](photo/(9)terra_public_sg.png)
![](photo/(9)terra_private_sg.png)

### 8. After showing all the above details, it summarised the work details in a line with output details which we can get for future use

![](photo/(10)plan_summary.png)

### Then execute the terraform apply command to create infrastructure on AWS 

![](photo/(11)terra_apply_1.png)
![](photo/(11)terra_apply_2.png)

---------------------------------------------------

## AWS Console Output

## ``` VPC ```
- eg : ninja-vpc-01

![](photo/aws_console/(1)VPC.png)

## ```Subnets ```

## (a) 2 public subnet
- eg : ninja-pub-sub-01/02

## (b) 2 private subnet
- eg : ninja-priv-sub-01/02 

![](photo/aws_console/(2)subnets.png)

## ``` NAT Gateway ```
- eg : ninja-nat-01 

![](photo/aws_console/(3)nat_gateway.png)

    
## ``` Internet Gateway ```
- eg : ninja-igw-01

![](photo/aws_console/(4)igw.png)


## ``` Create 2 Route Table ```
## (a) 1 for public subnet
- eg : ninja-route-pub-01/02 

![](photo/aws_console/(5)pub_rt_sub_associate_1.png)
![](photo/aws_console/(5)pub_rt_routes_2.png)

## (b) 1 for private subnet
- eg : ninja-route-priv-01/02 

![](photo/aws_console/(5)priv_rt_sub_associates_3.png)
![](photo/aws_console/(5)priv_rt_routes_4.png)

## ``` Create  ec2-instances based on your assigned software(cluster or HA) ```

- eg : mysql-master
- eg : mysql-slave-1

![](photo/aws_console/(6)ec2_instances.png)


## ```Create  Security Groups for  your EC2 instances```

### (a) port 22 of bastion host should only accessible from your public ip.
![](photo/aws_console/(7)bastion_sg.png)

### (b) port 22 of the private instances should only be accessible from  bastion host and port {Software_Port} of the private instances should only be accessible from bastion host

![](photo/aws_console/(7)master_sg.png)


# AUTHOR
[NIKHIL.PANCHAL]
