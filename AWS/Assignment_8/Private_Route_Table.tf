resource "aws_route_table" "PRIVATE_RT" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.MySQL_NAT_IG.id
  }

  tags = {
    Name = "MySQL_PRIVATE_RT"
  }
}

resource "aws_route_table_association" "PRIVATE_RT_1" {
  subnet_id      = aws_subnet.Private.id
  route_table_id = aws_route_table.PRIVATE_RT.id
}