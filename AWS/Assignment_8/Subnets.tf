resource "aws_subnet" "Private" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = var.SUB_PRIVATE_1_CIDR
  availability_zone = var.PRI_SUB
   tags = {
    "Name" = var.SUB_PRIVATE_1_NAME
  }
}

resource "aws_subnet" "Public" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = var.SUB_PUBLIC_1_CIDR
  availability_zone = var.PUB_SUB
   tags = {
    "Name" = var.SUB_PUBLIC_1_NAME
  }
}