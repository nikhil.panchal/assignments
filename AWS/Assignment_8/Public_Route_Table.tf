resource "aws_route_table" "PUBLIC_RT" {
  vpc_id = aws_vpc.main_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.MySQL_IG.id
  }

  tags = {
    Name = "MySQL_PUBLIC_RT"
  }
}

resource "aws_route_table_association" "PUBLIC_RT_1" {
  subnet_id      = aws_subnet.Public.id
  route_table_id = aws_route_table.PUBLIC_RT.id
}
