resource "aws_vpc" "main_vpc" {
  cidr_block = var.VPC_CIDR
  tags = {
    "Name" = var.VPC_Name
  }
}