variable "VPC_Name" { 
    default = "MYSQL_VPC" 
}

variable "VPC_CIDR" { 
    default = "10.0.0.0/16" 
}

variable "SUB_PRIVATE_1_CIDR" {
    default = "10.0.0.0/24"
}

#variable "SUB_PRIVATE_2_CIDR" {
#    default = "10.0.0.0/24" 
#}

variable "SUB_PUBLIC_1_CIDR" {
    default = "10.0.2.0/24"
}

#variable "SUB_PUBLIC_2_CIDR" {
#    default = "10.0.1.0/24"
#}

variable "SUB_PRIVATE_1_NAME" {
    default = "MySQL_SUB_PRI-1" 
}

variable "SUB_PRIVATE_2_NAME" {
    default = "MySQL_SUB_PRI-2" 
}

variable "SUB_PUBLIC_1_NAME" {
    default = "MySQL_SUB_PUB-1" 
}

variable "SUB_PUBLIC_2_NAME" {
    default = "MySQL_SUB_PUB-2" 
}


variable "IG_NAME" {
    default = "MySQL_IG" 
}

variable "PUB_SUB" {
    default = "ap-south-1a" 
}

variable "PRI_SUB" {
    default = "ap-south-1c" 
}