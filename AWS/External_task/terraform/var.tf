variable "Region" {
  default = "us-east-2"
}

variable "vpc_cidr" {
  description = "vpc id for subnet"
  default     = "10.0.0.0/22"
}

variable "vpc_name" {
  description = "vpc name"
  default     = "Sprin3Hibernate"
}

variable "public_subnet_cidr" {
  type    = list(any)
  default = ["10.0.0.0/25", "10.0.0.128/25"]
}

variable "private_subnet_app_cidr" {
  type    = list(any)
  default = ["10.0.1.0/25", "10.0.1.128/25"]
}

variable "private_subnet_data_cidr" {
  type    = list(any)
  default = ["10.0.2.0/25", "10.0.2.128/25"]
}

variable "azs" {
  type    = list(any)
  default = ["us-east-2a", "us-east-2b"]
}

variable "public_subnet_name" {
  description = "public subnet name"
  default     = "SP3_pub_sub"
}

variable "private_subnet_app_name" {
  description = "private subnet name"
  default     = "sp3_priv_app"
}

variable "private_subnet_data_name" {
  description = "private subnet name"
  default     = "sp3_priv_data"
}

variable "tags" {
  description = "A map of tags to be added for all"
  type        = map(string)
  default     = {}
}

variable "igw_name" {
  description = "Internet gateway name"
  default     = "sp3_igw_01"
}


variable "elastic_ip_name" {
  description = "elastic ip name"
  default     = "sp3_elastic_ip"
}

variable "nat_gateway_name" {
  description = "NAT gateway name"
  default     = "sp3_nat_01"
}


variable "rt_cidr_block" {
  description = "Route table cidr block"
  default     = "0.0.0.0/0"
}

variable "public_rt_name" {
  description = "public route tabel name"
  default     = "sp3_public_RT"
}

variable "private_rt_name" {
  description = "private route tabel name"
  default     = "sp3_private_RT"
}

variable "sp3_app_sg_name" {
  description = "application security group name"
  default     = "sp3_app_sg"
}

variable "sp3_data_sg_name" {
  description = "database security group name"
  default     = "sp3_data_sg"
}

variable "sp3_alb_sg_name" {
  description = "application loadbalancer security group name"
  default     = "sp3_alb_sg"
}

variable "Bashion_sg_name" {
  description = "bashion host security group"
  default     = "Bashion_sg"
}

variable "ami_id" {
  description = "us-east-2 ami id for instances"
  default     = "ami-0aeb7c931a5a61206"
}

variable "instance_type" {
  description = "instance type"
  default     = "t2.micro"
}

variable "key" {
  description = "pem key to authenticate the server"
  default     = "sp3"
}

variable "bastion_host_name" {
  description = "bastion host name"
  default     = "bastion_host"
}

variable "private_instance" {
  description = "count of private instances as per requirement"
  default     = 2
}

variable "app_instance_name" {
  description = "instance name"
  default     = ["sp3-1", "sp3-2"]
}

variable "mysql_instance_name" {
  description = "instance name"
  default     = ["mysql_master", "mysql_slave_1"]
}

variable "target_group_name" {
  description = "Target name for ALB"
  default     = "sp3-alb-Target"
}

variable "alb_name" {
  description = "name for ALB"
  default     = "sp3-alb"
}

variable "script" {
  description = "script fo installtion of tomcat"
  default     = <<-EOF
                  #!/bin/bash
                  sudo su
                  sudo apt-get update
                  sudo apt-get install nginx -y
                  sudo systemctl enable nginx
                  sudo systemctl start nginx
                  ip=`hostname -i`
                  echo "hie this is $ip " > /var/www/html/index.html
                  sudo systemctl resatrt nginx
                  EOF

}