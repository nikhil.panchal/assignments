# Routing table for public subnet 
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.sp3_vpc.id
  route {
    cidr_block = var.rt_cidr_block
    gateway_id = aws_internet_gateway.sp3-igw.id
  }
  tags = {
    Name = var.public_rt_name
  }
}

# Routing table for private subnet 
resource "aws_route_table" "private_rt" {
  vpc_id = aws_vpc.sp3_vpc.id
  route {
    cidr_block     = var.rt_cidr_block
    nat_gateway_id = aws_nat_gateway.sp3-nat.id
  }
  tags = {
    Name = var.private_rt_name
  }
}

# Route table associations for public
resource "aws_route_table_association" "public_association" {
  #count          = length(var.public_subnet_cidr)
  count          = length(var.public_subnet_cidr)
  subnet_id      = aws_subnet.sp3_pub_sub.*.id[count.index]
  route_table_id = aws_route_table.public_rt.id
}

# Route table associations for private subnet of app
resource "aws_route_table_association" "private_association_app" {
  count          = length(var.private_subnet_app_cidr)
  subnet_id      = aws_subnet.sp3_pri_app.*.id[count.index]
  route_table_id = aws_route_table.private_rt.id
}

# Route table associations for private subnet of database
resource "aws_route_table_association" "private_association_data" {
  count          = length(var.private_subnet_data_cidr)
  subnet_id      = aws_subnet.sp3_pri_data.*.id[count.index]
  route_table_id = aws_route_table.private_rt.id
}