# Bastion Host Instance 
resource "aws_instance" "bastion_host" {
  ami                    = var.ami_id
  instance_type          = var.instance_type
  count                  = 1
  subnet_id              = aws_subnet.sp3_pub_sub.*.id[count.index]
  key_name               = var.key
  vpc_security_group_ids = [aws_security_group.sp3_bashion_sg.id]


  tags = {
    Name = var.bastion_host_name
  }
}

# Appliciation Instance for frontend
resource "aws_instance" "App_instance" {
  ami                    = var.ami_id
  count                  = var.private_instance
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.sp3_pri_app.*.id[count.index]
  key_name               = var.key
  vpc_security_group_ids = [aws_security_group.sp3_app_sg.id]
  user_data              = var.script


  tags = {
    Name = var.app_instance_name[count.index]
  }
}

# mysql master and slave-1 Instance for database
resource "aws_instance" "Data_instance" {
  ami                    = var.ami_id
  count                  = var.private_instance
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.sp3_pri_data.*.id[count.index]
  key_name               = var.key
  vpc_security_group_ids = [aws_security_group.sp3_data_sg.id]

  tags = {
    Name = var.mysql_instance_name[count.index]
  }
}
