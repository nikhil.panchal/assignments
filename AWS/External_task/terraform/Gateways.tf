# Create internet gateway
resource "aws_internet_gateway" "sp3-igw" {
  vpc_id = aws_vpc.sp3_vpc.id

  tags = {
    Name = var.igw_name
  }
}

# Create elastic Ip
resource "aws_eip" "e_ip" {
  tags = {
    Name = var.elastic_ip_name
  }
}

# Create NAT gateway
resource "aws_nat_gateway" "sp3-nat" {
  allocation_id = aws_eip.e_ip.id
  subnet_id     = aws_subnet.sp3_pub_sub.*.id[0]
  tags = {
    Name = var.nat_gateway_name
  }
}