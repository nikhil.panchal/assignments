# Create public subnet  (sp3-pub-01/02)
resource "aws_subnet" "sp3_pub_sub" {
  vpc_id                  = aws_vpc.sp3_vpc.id
  map_public_ip_on_launch = true
  count                   = length(var.public_subnet_cidr)
  cidr_block              = var.public_subnet_cidr[count.index]
  availability_zone       = var.azs[count.index]

  tags = merge(
    {
      Name = format("%s-%d", var.public_subnet_name, count.index + 1)
    },
    var.tags,
  )
}

# Create public subnet for application (sp3-pri-app-01/02)
resource "aws_subnet" "sp3_pri_app" {
  vpc_id                  = aws_vpc.sp3_vpc.id
  map_public_ip_on_launch = true
  count                   = length(var.private_subnet_app_cidr)
  cidr_block              = var.private_subnet_app_cidr[count.index]
  availability_zone       = var.azs[count.index]

  tags = merge(
    {
      Name = format("%s-%d", var.private_subnet_app_name, count.index + 1)
    },
    var.tags,
  )
}

# Create public subnet for database  (sp3-pri-data-01/02)
resource "aws_subnet" "sp3_pri_data" {
  vpc_id                  = aws_vpc.sp3_vpc.id
  map_public_ip_on_launch = true
  count                   = length(var.private_subnet_data_cidr)
  cidr_block              = var.private_subnet_data_cidr[count.index]
  availability_zone       = var.azs[count.index]

  tags = merge(
    {
      Name = format("%s-%d", var.private_subnet_data_name, count.index + 1)
    },
    var.tags,
  )
}