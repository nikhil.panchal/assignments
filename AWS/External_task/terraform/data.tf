data "aws_availability_zones" "azs" {
  state = "available"
}

data "http" "myip" {
  url = "http://ipv4.icanhazip.com/"
}