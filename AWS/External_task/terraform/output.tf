output "sp3_vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.sp3_vpc.id
}

output "public_subnet_id" {
  description = "The ID of the Public Subnet"
  value       = aws_subnet.sp3_pub_sub.*.id
}

output "private_subnet_app_id" {
  description = "The ID of the Private Subnet"
  value       = aws_subnet.sp3_pri_app.*.id
}

output "private_subnet_data_id" {
  description = "The ID of the Private Subnet"
  value       = aws_subnet.sp3_pri_data.*.id
}

output "sp3-igw_id" {
  description = "The ID of the Private Subnet"
  value       = aws_internet_gateway.sp3-igw.id
}

output "sp3-nat-gatway_id" {
  description = "The ID of the NAT gateway"
  value       = aws_nat_gateway.sp3-nat.id
}

output "public_rt_id" {
  description = "Routing table for public subnet"
  value       = aws_route_table.public_rt.id
}


output "private_rt_id" {
  description = "Routing table for private subnet"
  value       = aws_route_table.private_rt.id
}

output "my_terraform_environmnet_public_ip" {
  description = "terraform environment public ip"
  value       = chomp(data.http.myip.body)
}

output "Bashion_sg_id" {
  description = "The ID of public security group"
  value       = aws_security_group.sp3_bashion_sg.id
}

output "app_sg_id" {
  description = "The ID of public security group"
  value       = aws_security_group.sp3_app_sg.id
}

output "data_sg_id" {
  description = "The ID of public security group"
  value       = aws_security_group.sp3_data_sg.id
}

output "alb_sg_id" {
  description = "The ID of public security group"
  value       = aws_security_group.sp3_alb_sg.id
}

output "bastion_host_detail" {
  description = "The ID of bastion host"
  value       = aws_instance.bastion_host.*.id
}

output "Application_host_detail" {
  description = "The ID of Application host"
  value       = aws_instance.App_instance.*.id
}

output "Datbase_host_detail" {
  description = "The ID of Database host"
  value       = aws_instance.Data_instance.*.id
}

output "alb_DNS" {
  description = "DNS of ALB"
  value = aws_alb.sp3_alb.dns_name
}

