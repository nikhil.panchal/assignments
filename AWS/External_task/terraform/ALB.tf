# Creating target group for ALB
resource "aws_alb_target_group" "sp3_target" {
  name     = var.target_group_name
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.sp3_vpc.id
}

# instances attached to Target group
resource "aws_alb_target_group_attachment" "Sp3_target_instance_attachment" {
  target_group_arn = aws_alb_target_group.sp3_target.arn
  count            = var.private_instance
  target_id        = aws_instance.App_instance.*.id[count.index]
  port             = 80
}

# create application load balancer
resource "aws_alb" "sp3_alb" {
  name            = var.alb_name
  subnets         = aws_subnet.sp3_pub_sub.*.id
  security_groups = [aws_security_group.sp3_alb_sg.id]
  internal        = false
  tags = {
    Name = var.alb_name
  }

}

resource "aws_alb_listener" "sp3_alb_listener" {
  load_balancer_arn = aws_alb.sp3_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.sp3_target.arn
    type             = "forward"
  }
}


