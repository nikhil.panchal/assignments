## Assignment 9

````
@here Ninja`s Please find the task:
## Infrastructure as a Code
**Note: Continuing from first assignment ......**
### Task Description
**Note: No hard coding of values is allowed in modules and resources**
- Convert your resources into modules and call them in a main file.
- Make use of interpolations and functions for creating multiple resources.
- Statefile should not be present with your code.
- Create jenkins pipeline that will create the Infra
    - There should be  approval stage before the infra is created
    -  Your job should send notification for status of job in slack.
- Create jenkins pipeline that will destroy the Infra
    - There should be  approval stage before the infra is Destroyed
    - Your job should send notification for status of job in slack.


````

## Task 
## After Creating all modules under the respective directory, it's the tree structure looks like below

![](photo/1.png)

## Execute terraform init commands inside your workspace
- The terraform init command is used to initialize a working directory containing terraform files.

![](photo/2.png)

## Then execute terraform plan command after the initializing the working directory
- The terraform plan command creates an execution plan, which lets you preview the changes that Terraform plans to make to your infrastructure. 

![](photo/4.png)

### 1. VPC

![](photo/(4)1_vpc.png)

### 2. Subnets

![](photo/(4)1a_subnet.png)
![](photo/(4)1b_subnet.png)
![](photo/(4)1c_subnet.png)
![](photo/(4)1d_subnet.png)

### 3. NAT Gateway

![](photo/(5)0_eip.png)
![](photo/(5)terra_nat_gateway.png)

### 4. Internet Gateway

![](photo/(6)terra_igw.png)


### 5. Route Table

### ``` public subnet route tables```
![](photo/(7)1_rt_pub.png)
![](photo/(7)2_rt_pub_associaltion.png)

### ``` private subnet route tables```
![](photo/(7)3_rt_priv.png)
![](photo/(7)4_rt_priv_association.png)

### 6. Instances

![](photo/(8)1_bastion_host.png)
![](photo/(8)2_mongo_master.png)
![](photo/(8)2_mongo_slave-1.png)

### 7. Security group

![](photo/(9)terra_public_sg.png)
![](photo/(9)terra_private_sg.png)

### 8. After showing all the above details, it summarised the work details in a line with output details which we can get for future use

![](photo/(10)plan_summary.png)

### Then execute the terraform apply command to create infrastructure on AWS 

![](photo/(11)terra_apply_1.png)
![](photo/(11)terra_apply_2.png)

---------------------------------------------------

## AWS Console Output

## ``` VPC ```
- eg : ninja-vpc-01

![](photo/aws_console/(1)VPC.png)

## ```Subnets ```

## (a) 2 public subnet
- eg : ninja-pub-sub-01/02

## (b) 2 private subnet
- eg : ninja-priv-sub-01/02 

![](photo/aws_console/(2)subnets.png)

## ``` NAT Gateway ```
- eg : ninja-nat-01 

![](photo/aws_console/(3)nat_gateway.png)

    
## ``` Internet Gateway ```
- eg : ninja-igw-01

![](photo/aws_console/(4)igw.png)


## ``` Create 2 Route Table ```
## (a) 1 for public subnet
- eg : ninja-route-pub-01/02 

![](photo/aws_console/(5)pub_rt_sub_associate_1.png)
![](photo/aws_console/(5)pub_rt_routes_2.png)

## (b) 1 for private subnet
- eg : ninja-route-priv-01/02 

![](photo/aws_console/(5)priv_rt_sub_associates_3.png)
![](photo/aws_console/(5)priv_rt_routes_4.png)

## ``` Create  ec2-instances based on your assigned software(cluster or HA) ```

- eg : mysql-master
- eg : mysql-slave-1

![](photo/aws_console/(6)ec2_instances.png)


## ```Create  Security Groups for  your EC2 instances```

### (a) port 22 of bastion host should only accessible from your public ip.
![](photo/aws_console/(7)bastion_sg.png)

### (b) port 22 of the private instances should only be accessible from  bastion host and port {Software_Port} of the private instances should only be accessible from bastion host

![](photo/aws_console/(7)master_sg.png)

## ```Create jenkins pipeline that will create and destroy the Infra```
NOTE: There should be approval stage before the infra is created/destroyed

### create git repository with jenkinsfile to run pipeline project with terraform modules

![](photo/jenkins/(5)terraform_git_repo_info.png)
![](photo/jenkins/(6)jenkinsfile.png)


### Create Jenkins pipeline project with below information
![](photo/jenkins/(1)project_parameter.png)
![](photo/jenkins/(2)project_git_scm.png)
![](photo/jenkins/(3)project_branch_and_script_path.png)

### Download "cloudBees AWS credential plugin" to allow AWS user credential within Jenkins credential API
![](photo/jenkins/(4)aws_jenkins_credential.png)

### after execute the action pipeline will start to continue to perform the steps


### job will send notification for status of job in slack.
![](photo/jenkins/(7)slack_notifiction.png)


# AUTHOR
[NIKHIL.PANCHAL]

