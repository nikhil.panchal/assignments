terraform {
  backend "s3" {
    bucket = "terrabucketsm"
    key    = "ninjatask/terraform.tfstate"
    region = "us-east-2"
  }
}