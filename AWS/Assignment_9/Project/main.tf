module "vpc_module" {
  source   = "../modules/vpc"
  vpc_cidr = var.vpc_cidr
  vpc_name = var.vpc_name
}

module "subnet_module" {
  source              = "../modules/subnet"
  ninja_vpc1          = module.vpc_module.ninja_vpc
  azs                 = var.azs
  public_subnet_cidr  = var.public_subnet_cidr
  private_subnet_cidr = var.private_subnet_cidr
  public_subnet_name  = var.public_subnet_name
  private_subnet_name = var.private_subnet_name
  tags                = var.tags
}

module "igw_nat_module" {
  source         = "../modules/gateway"
  ninja_vpc1     = module.vpc_module.ninja_vpc
  ninja_igw_name = var.ninja_igw_name
  e_ip_name      = var.e_ip_name
  ninja_nat_name = var.ninja_nat_name
  public_subnet  = module.subnet_module.public_subnet_id[0]
}

module "route_module" {
  source              = "../modules/route"
  ninja_vpc1          = module.vpc_module.ninja_vpc
  rt_cidr_block       = var.rt_cidr_block
  internet_gateway_id = module.igw_nat_module.ninja-igw
  nat_gateway_id      = module.igw_nat_module.ninja-nat

  public_subnet  = module.subnet_module.public_subnet_id
  public_rt_name = var.public_rt_name

  private_subnet  = module.subnet_module.private_subnet_id
  private_rt_name = var.private_rt_name
}

module "securitygroup_module" {
  source                   = "../modules/securitygroup"
  ninja_vpc1               = module.vpc_module.ninja_vpc
  public_sg_name           = var.public_sg_name
  private_sg_name          = var.private_sg_name
  public_sg_port           = var.public_sg_port
  private_sg_ports_ingress = var.private_sg_ports_ingress
  public_subnet_cidr_block = var.public_subnet_cidr
  sg_protocol              = var.sg_protocol
}

module "ec2_module" {
  source             = "../modules/ec2"
  ami_id             = var.ami_id
  instance_type      = var.instance_type
  public_subnet_cidr = var.public_subnet_cidr
  public_subnet      = module.subnet_module.public_subnet_id
  private_subnet     = module.subnet_module.private_subnet_id
  key_n              = var.key_n
  public_sg          = module.securitygroup_module.public_sg
  private_sg         = module.securitygroup_module.private_sg
  bastion_host_name  = var.bastion_host_name
  private_instance   = var.private_instance
  instance_name      = var.instance_name
}
