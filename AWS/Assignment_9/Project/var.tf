variable "vpc_cidr" {
  description = "vpc id for subnet"
  default     = "10.0.0.0/16"
}

variable "vpc_name" {
  description = "vpc name"
  default     = "ninja-vpc-01"
}

variable "public_subnet_cidr" {
  type    = list(any)
  default = ["10.0.0.0/19", "10.0.32.0/19"]
}

variable "private_subnet_cidr" {
  type    = list(any)
  default = ["10.0.64.0/18", "10.0.128.0/17"]
}

variable "azs" {
  type    = list(any)
  default = ["us-east-2a", "us-east-2b"]
}

variable "public_subnet_name" {
  description = "public subnet name"
  default     = "ninja-pub-sub"
}

variable "private_subnet_name" {
  description = "private subnet name"
  default     = "ninja-priv-sub"
}

variable "tags" {
  description = "A map of tags to be added for all"
  type        = map(string)
  default     = {}
}

variable "ninja_igw_name" {
  description = "Internet gateway name"
  default     = "ninja-igw-01"
}

variable "e_ip_name" {
  description = "elastic ip name"
  default     = "ninja-elastic-ip"
}

variable "ninja_nat_name" {
  description = "NAT gateway name"
  default     = "ninja-nat-01"
}

variable "rt_cidr_block" {
  description = "Route table cidr block"
  default     = "0.0.0.0/0"
}

variable "public_rt_name" {
  description = "public subnet name"
  default     = "ninja-route-pub"
}

variable "private_rt_name" {
  description = "private subnet name"
  default     = "ninja-route-priv"
}

variable "public_sg_port" {
  description = "public security group port to access bastion host"
  default     = 22
}

variable "private_sg_ports_ingress" {
  description = "private security group ingress ports"
  default     = [22, 27017]
}

variable "sg_protocol" {
  description = "private security group ingress protocol"
  default     = "tcp"
}

variable "public_sg_name" {
  description = "public security group"
  default     = "ninja-public-sg"
}

variable "private_sg_name" {
  description = "private security group"
  default     = "ninja-private-sg"
}

variable "ami_id" {
  description = "us-east-2 ami id for instances"
  default     = "ami-064ff912f78e3e561"
}

variable "instance_type" {
  description = "instance type"
  default     = "t2.micro"
}

variable "key_n" {
  description = "pem key to authenticate the server"
  default     = "ohio"
}

variable "bastion_host_name" {
  description = "bastion host name"
  default     = "bastion_host"
}

variable "private_instance" {
  description = "count of private instances as per requirement"
  default     = 2
}

variable "instance_name" {
  description = "instance name"
  default     = ["mongo-master", "mongo-slave-1"]
}