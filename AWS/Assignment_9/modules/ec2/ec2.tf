# Bastion Host Instance 
resource "aws_instance" "bastion_host" {
  ami           = var.ami_id
  instance_type = var.instance_type
  count     = 1
  subnet_id = var.public_subnet[count.index]
  key_name  = var.key_n
  vpc_security_group_ids = [var.public_sg]

  tags = {
    Name = var.bastion_host_name
  }
}

# MongoDB master and slave-1 Instance 
resource "aws_instance" "private_instance" {
  ami           = var.ami_id
  count         = var.private_instance
  instance_type = var.instance_type
  subnet_id     = var.private_subnet[count.index]
  key_name      = var.key_n
  vpc_security_group_ids = [var.private_sg]

  tags = {
    Name = var.instance_name[count.index]
  }
}
