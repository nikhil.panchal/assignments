output "bastion_host" {
  description = "The ID of bastion host"
  value = aws_instance.bastion_host[0]
}

output "private_instance" {
  description = "The ID of private instances"
  value = aws_instance.private_instance.*.id
}

