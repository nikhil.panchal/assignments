variable "ami_id" {
  description = "us=east-2 ami id for instances"
}

variable "instance_type" {
  description = "instance type"
}

variable "public_subnet_cidr" {
  description = "public subnet cidr"
}

variable "public_subnet" {
  description = "public subnet id"
}

variable "private_subnet" {
  description = "private subnet id"
}

variable "key_n" {
  description = "pem key to authenticate the server"
}

variable "public_sg" {
  description = "public security group"
}

variable "private_sg" {
  description = "private security group"
}

variable "bastion_host_name" {
  description = "bastion host name"
}

variable "private_instance" {
  description = "count of private instances as per requirement"
}

variable "instance_name" {
  type    = list(any)
  description = "private instance name"
}