variable "ninja_vpc1" {
    description = "VPC CIDR"
}

variable "public_subnet" {
  description = "NAT gateway"
}

variable "ninja_igw_name" {
  description = "Internet gateway name"
}

variable "e_ip_name" {
 description = "elastic ip name" 
}

variable "ninja_nat_name" {
  description = "NAT gateway name"
}