output "ninja-igw" {
  description = "The ID of the Private Subnet"
  value       = aws_internet_gateway.ninja-igw.id
}

output "ninja-nat" {
  description = "The ID of the NAT gateway"
  value       = aws_nat_gateway.ninja-nat.id
}