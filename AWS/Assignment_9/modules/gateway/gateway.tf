# Create internet gateway
resource "aws_internet_gateway" "ninja-igw" {
  vpc_id                  = var.ninja_vpc1

  tags = {
    Name = var.ninja_igw_name
  }
}

# Create elastic Ip
resource "aws_eip" "e_ip" {
  tags = {
    Name = var.e_ip_name
  }
}

# Create NAT gateway
resource "aws_nat_gateway" "ninja-nat" {
  allocation_id = aws_eip.e_ip.id
  subnet_id     = var.public_subnet
  tags = {
    Name = var.ninja_nat_name
  }
}