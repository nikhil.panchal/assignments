# Create public subnet  (ninja-pub-sub-01/02)
resource "aws_subnet" "public_subnet_id" {
  vpc_id                  = var.ninja_vpc1
  map_public_ip_on_launch = true
  count                   = length(var.public_subnet_cidr)
  cidr_block              = var.public_subnet_cidr[count.index]
  availability_zone       = var.azs[count.index]

  tags = merge(
    {
      Name = format("%s-%d", var.public_subnet_name,count.index+1)
    },
    var.tags,
  )
}

# Create private subnet  (ninja-priv-sub-01/02)
resource "aws_subnet" "private_subnet_id" {
  vpc_id                  = var.ninja_vpc1
  map_public_ip_on_launch = false
  count                   = length(var.private_subnet_cidr)
  cidr_block              = var.private_subnet_cidr[count.index]
  availability_zone       = var.azs[count.index]

  tags = merge(
    {
      Name = format("%s-%d", var.private_subnet_name,count.index+1)
    },
    var.tags,
  )
}
