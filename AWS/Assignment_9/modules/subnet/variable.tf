variable "public_subnet_cidr" {
  description = "public subnet cidr"
}

variable "private_subnet_cidr" {
  description = "private subnet cidr"
}

variable "azs" {
  description = "Subnet Availability Zone"
}

variable "ninja_vpc1" {
  description = "VPC CIDR"
}

variable "public_subnet_name" {
  description = "public subnet name"
}

variable "private_subnet_name" {
  description = "private subnet name"
}

variable "tags" {
  description = "tags for subnet name"
}
