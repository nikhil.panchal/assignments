# Routing table for public subnet 
resource "aws_route_table" "public_rt" {
  vpc_id = var.ninja_vpc1
  route{
     cidr_block = var.rt_cidr_block
     gateway_id = var.internet_gateway_id
  }
  tags = {
    Name = var.public_rt_name
  }
}

# Route table associations for public
resource "aws_route_table_association" "public_association" {
  #count          = length(var.public_subnet_cidr)
  count          = length(var.public_subnet)
  subnet_id      = var.public_subnet[count.index]
  route_table_id = aws_route_table.public_rt.id
}

# Routing table for private subnet 
resource "aws_route_table" "private_rt" {
  vpc_id = var.ninja_vpc1
  route {
    cidr_block = var.rt_cidr_block
    nat_gateway_id = var.nat_gateway_id
  }
  tags = {
    Name = var.private_rt_name
  }
}

# Route table associations for private
resource "aws_route_table_association" "private_association" {
  #count          = length(var.private_subnet_cidr)
  count          = length(var.private_subnet)
  subnet_id      = var.private_subnet[count.index]
  route_table_id = aws_route_table.private_rt.id
}