output "public_rt" {
  description = "Routing table for public subnet"
  value = aws_route_table.public_rt.id
}


output "private_rt" {
  description = "Routing table for private subnet"
  value = aws_route_table.private_rt.id
}
