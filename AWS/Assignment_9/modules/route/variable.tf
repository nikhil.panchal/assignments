variable "ninja_vpc1" {
  description = "VPC CIDR"
}

variable "rt_cidr_block" {
  description = "Route table cidr block"
}

variable "internet_gateway_id" {
  description = "internet gateway id for route table"
}

variable "nat_gateway_id" {
  description = "nat gateway id for route table"
}

variable "public_rt_name" {
  description = "public route table name"
}

variable "private_rt_name" {
  description = "private route table name"
}

# variable "public_subnet_cidr" {
#   description = "public subnet cidr"
# }

variable "public_subnet" {
  description = "public subnet id for public route table"
}

# variable "private_subnet_cidr" {
#   description = "private subnet cidr"
# }

variable "private_subnet" {
  description = "private subnet id for private route table"
}

