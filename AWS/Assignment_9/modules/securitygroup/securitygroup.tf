# Get current IP, can be used by ${data.http.myip.body}
data "http" "myip" {
  url = "http://ipv4.icanhazip.com/"
}


# public security group
resource "aws_security_group" "public_sg" {
  vpc_id = var.ninja_vpc1
  ingress {
    from_port   = var.public_sg_port
    to_port     = var.public_sg_port
    protocol    = var.sg_protocol
    #cidr_blocks = ["${chomp(data.http.myip.body)}/32"]
    cidr_blocks = ["${chomp(data.http.myip.body)}/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.public_sg_name
  }
}

# private security group
resource "aws_security_group" "private_sg" {
  vpc_id = var.ninja_vpc1
  dynamic "ingress" {
    for_each = var.private_sg_ports_ingress
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = var.sg_protocol
      cidr_blocks = var.public_subnet_cidr_block
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.private_sg_name
  }
}