output "my_terraform_environmnet_public_ip" {
  description = "terraform environment public ip"
  value = "${chomp(data.http.myip.body)}"
}

output "public_sg" {
  description = "The ID of public security group"
  value = aws_security_group.public_sg.id
}

output "private_sg" {
  description = "The ID of private security group"
  value = aws_security_group.private_sg.id
}