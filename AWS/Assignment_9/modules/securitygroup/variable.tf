variable "ninja_vpc1" {
  description = "VPC CIDR"
}

variable "public_sg_name" {
  description = "public security group"
}

variable "private_sg_name" {
  description = "private security group"
}
  
variable "public_sg_port" {
  description = "public security group port to access bastion host"
}

variable "private_sg_ports_ingress" {
  type    = list(any)
  description = "private security group ingress ports"
}

variable "sg_protocol" {
  description = "private security group ingress protocol"
}

variable "public_subnet_cidr_block" {
  description = "public subnet cidr block"
}