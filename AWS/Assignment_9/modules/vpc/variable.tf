variable "vpc_cidr" {
  description = "VPC CIDR"
}

variable "vpc_name" {
  description = "VPC Name"
}