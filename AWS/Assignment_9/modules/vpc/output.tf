output "ninja_vpc" {
  description = "The ID of the VPC"
  value       = aws_vpc.ninja_vpc.id
}
