___________________________________
# ASSIGNMENT 1 : AWS
____________________________________
````
DAY 1
What you will learn from this lab

VPC
Internet Gateways
NAT Gateways
Subnets
Route Tables
EC2 Instances
Security group


Task

Create 1 VPC
eg : ninja-vpc-01

Create 1 IGW
eg : ninja-igw-01

Create 1 NAT
eg : ninja-nat-01



Create 2 Subnet
1 public subnet
eg : ninja-pub-sub-01


1 private subnet
eg : ninja-priv-sub-01





Create 2 Route Table

1 for public subnet
eg : ninja-route-pub-01


1 for private subnet
eg : ninja-route-priv-01


Create 2 ec2-instance

1 in public subnet
prefered ami : ubuntu
eg : ninja-ec2-pub-01


1 in private subnet
prefered ami : ubuntu
eg : ninja-ec2-priv-01





Create a Security Group

security group should be open for specific ip only


Ping ninja-ec2-priv-01  from ninja-ec2-pub-01  and vice-versa


Instructions to perform this lab

Perform all the steps
Once you have performed all the steps  then make a Readme Doc with step by step representation of all the things you have done and attach screenshot in each step with explaination.


Good to Do

Perform all the steps through AWS CLI.
````

# TASK-1 : Creating VPC, IGW, NAT

## STEP-1 Creating VPC

1. So to create VPC we will first log-in to AWS account.
2. Find VPC service in search bar and launch it.

![](photo/vpc.png)

3. Now select my VPC

![](photo/Select_VPC.png)

4. Here you will find create VPC option select it.

![](photo/create_vpc.png)

5. There are two way we can create VPC 
    1. VPC only : other component we have to manually create.
    2. VPC with subnet etc : all component will be created automaticaly for VPC.

![](photo/VPC_type.png)

for this lab we will create only VPC and after that create other component for it

6. so we will create VPC with following detail.
    1. Name : ninja-vpc-01
    2. CIDR : 10.0.0.0/24

![](photo/vpc_created.png)

## STEP-2 Creating IGW

1. Go to Internet Gateways tab and there you will fing create Internet Gateways option select it.
![](photo/Create_IGW.png)

2. so we will create IGW with following detail.
    1. Name : ninja-igw-01
![](photo/IGW.png)

3. click on create and IGW will be created successfuly

![](photo/IGW_complete.png)

4. we will attach IGW to VPC

![](photo/attech_IGW.png)

## STEP-3 Creating NAT

1. Go to NAT Gateways tab and there you will fing create NAT Gateways option select it.
![](photo/NAT.png)

2. so we will create IGW with following detail.
    1. Name : ninja-nat-01
    2. Subnet : as we create NAT it need public subnet which it will use to connect with private subnet.
    3. Elastic IP allocation ID : it will be automatically created with allocate elastic ip tab and its static ip.

![](photo/Create_NAT.png)

3. click on create and NAT will be created successfuly

![](photo/NAT_done.png)

# TASK-2 : Creating Subnet private and public

## STEP-1 Creating Subnet

1. go to subnet tab and click on create subnet.
![](photo/subnet_create.png)

2. now we have to select VPC in which we want to create subnet.
{**note:** subnet always created under VPC }

![](photo/Select_VPC.png)

3. now we will add following detail forsubnet
**note:** CIDR value of subnet will always be smaller than VPC CIDR value.


**FOR PUBLIC SUBNET**
   1. name : ninja-pub-sub-01
   2. Availability Zone : ap-south-1a
   3. CIDR : 10.0.0.0/28 
![](photo/public_subnet.png)
    
**FOR PRIVATE SUBNET**
   1. name : ninja-priv-sub-01
   2. Availability Zone : ap-south-1a
   3. CIDR : 10.0.0.16/28 
    (as we have allocated public subnet 16 ips so priavate subnet CIDR start with 10.0.0.16)
![](photo/private_subnet.png)


# TASK-3 : Creating Route Table

1. Go to Route Table tab and select create Route Table
![](photo/Route_table.png)

2. Now add name, select VPC and click on create table and route table will be created.
![](photo/pub_route_table.png)

3. same as above we have created two route table one public and one private.
![](photo/two_route_table.png)

4. for public route table we will attach public subnet and IGW.
    1. associating subnet to route table
![](photo/pub_sub_associate.png)
    2. attching new rule for IGW
![](photo/add_IGW_to_RT.png)


5. for private route table we will attach private subnet and NAT.
    1. associating subnet to route table
![](photo/pri_sub_associate.png)
    2. attching new rule for NAT
![](photo/attach_nat_to_RT.png)

# TASK-4 : Creating Security Group

1. Go to Security Group and click on create Security Group
![](photo/SG_create.png)

2. Add name for Security Group and select VPC for Security Group
![](photo/SG.png)

3. Now we will add inbound rule for accesing specific ip only and for ping give one ICMP rule.
![](photo/SG_rule.png)

4. click on create and Security Group will be created.
![](photo/SG_done.png)


# TASK-5 : Creating ec2-instance

1. first search ec2 service in search tab.
![](photo/search_ec2.png)

2. open ec2 service and click on launch instance
![](photo/launch_instance.png)

3. So first we will create public instance.
   now instance launcher will open and there are total 7 steps for this.
    1. Step 1: Choose an Amazon Machine Image (AMI) : ubuntu
    ![](photo/step1.png)
    2. Step 2: Choose an Instance Type : t2 micro
    ![](photo/step2.png)
    3. Step 3: Configure Instance Details : we will select VPC we have created and public subnet for it.
    ![](photo/step3.png)
    4. Step 4: Add Storage : we will add 30 GB
    ![](photo/step4.png)
    5. Step 5: Add Tags : name = ninja_public
    ![](photo/step5.png)
    6. Step 6: Configure Security Group : we will add SG we just created.
    ![](photo/step6.png)
    7. Step 7: Review Instance Launch and click on launch.
    ![](photo/step7.png)

4. as we have created public instance we will create private instance. only difference is that we will select private subnet fot this.

so we have created two instance as below.
![](photo/instance_done.png)

after this it will ask for key-pair you will have to download the key and every u want to launch instance u will use this key.
![](photo/key_pair.png)


# task-6 : Ping ninja-ec2-priv-01  from ninja-ec2-pub-01  and vice-versa

## Trial-1 : ping ninja-ec2-pub-01 from local 
 so fot this we will use ping command and give public ec2's public ip
![](photo/ping_local_pub.png)

## Trial-2 : ping ninja-ec2-priv-01  from ninja-ec2-pub-01

1. open command line and go to the directory where key file is downloaded.
![](photo/key.png)
2. use this command.
```
$ ssh -i {key_file_name} ubuntu@{public_ip_of_instance}
```

3. here we have launch the public instance in local 
![](photo/public_instance.png)

4. we will ping private ec2 by giving private ip of private ec2
![](photo/ping_pub_pri.png)

## Trial-3 : ping ninja-ec2-pub-01  from ninja-ec2-priv-01
Select an instance
￼

1. ssh to private ec2
2. we will ping public ec2 by giving public ip of public ec2

![](photo/ping_pri_pub.png)



## **AUTHOR**
[NIKHIL.PANCHAL]



    

