# ASSIGNMENT 7 : AWS
_________________________________
`````
@here, Good Morning Ninja`s Please find today`s assignment !!
## What you will learn from this lab
- S3
- Hosting a static site in s3
- Cloudfront (CDN)
- Route 53
## Task Description
- Create 2 S3 buckets
    -  With console (bucket name = ninja)
    -  With CLI (bucket name= BuddyName)
- create 2 index.html page
    - With content "Hi ninja"
    - With "Hi buddy name"
- Upload index.html with "Hi ninja" using AWS console on ninja bucket.
- Upload index.html inside folder 'ninja with "Hi Buddy" using AWS CLI  on BuddyName bucket.
- Configure CloudFront for both s3 buckets in such a fashion that CloudFront should open "Hi ninja" on default and cloudfront dns/ninja should display "Hi buddy"
 **S3 sxxxxxxhould not be publicly open (Only accessible through CloudFront using bucket policy and OAI)**
- Arrange a domain from any registrar(eg: try to get it free or cheapest ;-) )
- Create a public hosted zone with the existing domain
- Now create a recordset  for mapping your  CloudFront DNS with {your_name}.{your_domain} which is hosting your previously created website
    - eg: Pankaj.crazy traveller.ga should open your website. (where "Pankaj" is name and "dcrazytraveller.ga" is your domain)
##  Good To Do
- Create a private hosted zone (choice of your domain) and attach it to your VPC.
- Create a EC2 machine and install nginx on it.(Can use default page or customize for fun ;-)  )
- Now create a recordset  for mapping your EC2 IP with {your_buddy_name}.{your_domain} which is hosting your previously created website
- Do the same activity as done in the public-hosted zone.
    - Now try accessing it from your local machine.
    - Create an EC2 instance in your VPC and try accessing the same domain from your machine.

`````
# Topic -1
____________________

# TASK-1 : Create 2 S3 buckets

1. creating s3 bucket name : ninja-14

    ![](photo/S3_ninja-14_bucket.png)

2. creating s3 bucket name : mrituyanjay-buddy with CLI
    
    ![](photo/S3_mrituyanjay_bucket.png)


    ## so both Bucket is Ready
    ![](photo/Bucket_ready.png)

# TASK-2 : - create 2 index.html page
1. With content "Hi ninja"

    ![](photo/hie_ninja.png)

2. With "Hi buddy name"
    ![](photo/Hi_buddy.png)


# TASK-3 : - Upload index.html

1. Upload index.html with "Hi ninja" using AWS console on ninja bucket.
    ![](photo/NInja_File.png)

2. Upload index.html inside folder 'ninja with "Hi Buddy" using AWS CLI  on BuddyName bucket.
    ![](photo/Buddy_file.png)

# Task-4 : Configure CloudFront for both s3 buckets in such a fashion that CloudFront should open "Hi ninja" on default and cloudfront dns/ninja should display "Hi buddy"

1. bucket : ninja-14

    1. Go to Cloudwatch Service
    2. Create distributor

