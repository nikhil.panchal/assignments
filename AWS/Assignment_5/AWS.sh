#!/bin/bash
#set -x

source Function.sh
source config.txt

Start
VPC_Create
Internet_gateway_create
Attach_IGW_To_VPC
Subnet_Create
NAT_Gateway_Create
Route_Table_create
Public_Route_Table_Configuration
Private_Route_Table_Configuration
Bastion_Host
Server_Launch
END


