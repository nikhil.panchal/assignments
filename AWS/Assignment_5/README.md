# ASSIGNMENT 5 : AWS
____________________________________

````
Day:5

Please find this Activity as today`s assignment:
-----------------------------------------------------------------
Architecture diagram -- Design a Architecture diagram of your Tool
2 GUI Setup - Setup your tool Architecture in AWS using AWS GUI
3 CLI Setup - Setup your tool Architecture in AWS using AWS CLI
4 Script - Design a script to setup your Tool

````
# Task:1 

## Architecture diagram -- Design a Architecture diagram of MySQL

![](photo/MySQL_Design.png)


--------------------------------------------------------------

# Task 2
## GUI Setup - Setup your tool Architecture in AWS using AWS GUI

## step-1 First, we will create VPC

![](photo/GUI/(1)VPC_created.png)

## step-2 Create Subnets

![](photo/GUI/(3)subnets_created.png)

## step-3 Create Internet gateway

![](photo/GUI/(4)igw_created.png)

## step-4 Create NAT gateway

![](photo/GUI/(5)NAT_gateway_created_for_pub_sub_01.png)

## step-5 Create Route table for Public Subnet

![](photo/GUI/(6)pub_sub_01_route_table_route.png)

## step-6 Subnet association for private route table

![](photo/GUI/(9)pri_slave2_sub_03_route_table_subnet_association.png)

## step-7 mysql bastion host created

![](photo/GUI/(10)mongo_bastion_host_created.png)

## step-8 mysql private master slave instance created

![](photo/GUI/(11)mongo_priv_master_slave_instance_created.png)

## step-9 mysql public security group created

![](photo/GUI/(12)mongo_pub_sg_created.png)

## step-10 mysql private security group created

![](photo/GUI/(13)mongo_pri_sg_created.png)

## step-11 Now login into Mysql bastion host

![](photo/GUI/(14)login_into_mongo_bastion_host.png)

----------------------------------------------------

## TASK CLI Setup - Setup your tool Architecture in AWS using AWS CLI and Design a script to setup your Tool

## RESULT

1. Script Created.

## Author
[@nikhil.panchal]
