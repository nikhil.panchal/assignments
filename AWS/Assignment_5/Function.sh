function Start(){
    echo "Initializing process.........."
    echo "creating VPC in $region region"
    echo "=======================================================" >> Last_Build_detail
    Date=`date`
    USER=`whoami`
    echo "DATE : $Date" >> Last_Build_detail
    echo "user : $USER" >> Last_Build_detail
}

function VPC_Create(){
    VPC_ID=$(aws ec2 create-vpc --cidr-block $VPC_CIDR --region $region --query Vpc.VpcId --output text --tag-specifications ResourceType=vpc,Tags='[{Key=Name,Value="$VPC_Name"}]')
   
    echo " $VPC_Name ID : $VPC_ID" >> Last_Build_detail
    echo "vpc created :$VPC_ID"
}

function Internet_gateway_create(){
    echo "Creating Internet Gateway"

    IGW_ID=$(aws ec2 create-internet-gateway --region $region --query InternetGateway.InternetGatewayId | tr -d '"')
    echo "Internate_Gateway_ID : $IGW_ID" >> Last_Build_detail
    echo "Internate Gateway Created : $IGW_ID"
}

function Attach_IGW_To_VPC(){
    echo "attaching Internet Gateway to VPC"
    aws ec2 attach-internet-gateway --vpc-id ${VPC_ID} --internet-gateway-id ${IGW_ID} --region $region
    echo "successfully Internet Gateway attached to VPC"
}

function Subnet_Create(){
    # Public Subnets
    echo "Public Subnet creating........."
    Sub_Pub_ID=$(aws ec2 create-subnet --vpc-id $VPC_ID --cidr-block $SUB_CIDR_1 --region $region --availability-zone $AZ_1 --query Subnet.SubnetId | tr -d '"')
    echo "Public Subnet created ${Sub_Pub_ID}"
    
    # Private Subnets
    echo "Multiple Private Subnets creating............."
    Sub_PRI-1_ID=$(aws ec2 create-subnet --vpc-id $VPC_ID --cidr-block $SUB_CIDR_2 --region $region --availability-zone $AZ_2 --query Subnet.SubnetId | tr -d '"')
    echo "Private Subnet-1 created ${Sub_PRI-1_ID}"
    Sub_PRI-2_ID=$(aws ec2 create-subnet --vpc-id $VPC_ID --cidr-block $SUB_CIDR_3 --region $region --availability-zone $AZ_3 --query Subnet.SubnetId | tr -d '"')
    echo "Private Subnet-2 created ${Sub_PRI-2_ID}"
    Sub_PRI-3_ID=$(aws ec2 create-subnet --vpc-id $VPC_ID --cidr-block $SUB_CIDR_4 --region $region --availability-zone $AZ_4 --query Subnet.SubnetId | tr -d '"')
    echo "Private Subnet-3 created ${Sub_PRI-3_ID}"
    echo "$SUB_Name_PUB :${Sub_Pub_ID}" >> Last_Build_detail
    echo "$SUB_Name_PRI1 : ${Sub_PRI-1_ID}" >> Last_Build_detail
    echo "$SUB_Name_PRI2 : ${Sub_PRI-2_ID}" >> Last_Build_detail
    echo "$SUB_Name_PRI3 : ${Sub_PRI-3_ID}" >> Last_Build_detail
}

function NAT_Gateway_Create(){
    echo "creating elastic-IP"
    Elastic_IP=$(aws ec2 allocate-address --region us-west-2 --query AllocationId | tr -d '"')
    NAT_Gateway_ID=$(aws ec2 create-nat-gateway --subnet-id ${Sub_Pub_ID} --allocation-id $Elastic_IP --region $region --query NatGatewayId | tr -d '"')
    echo "NAT Gateway Created $NAT_Gateway_ID"
    echo "NAT_Gateway_ID : $NAT_Gateway_ID" >> Last_Build_detail 
}

function Route_Table_create(){
    echo "creating public Route Table"
    PUB_MySQL_RT = $(aws ec2 create-route-table --vpc-id $VPC_ID --region $region --query RouteTable.RouteTableId --output text | tr -d '"')
    PRI_MySQL_RT = $(aws ec2 create-route-table --vpc-id $VPC_ID --region $region --query RouteTable.RouteTableId --output text | tr -d '"')
    echo "PUB_MySQL_RT : ${PUB_MySQL_RT}" >> Last_Build_detail
    echo "PRI_MySQL_RT : ${PRI_MySQL_RT}" >> Last_Build_detail
    echo "Two Route Tabel Created
            1. Public : ${PUB_MySQL_RT}
            2. Private : ${PRI_MySQL_RT}"

}

function Public_Route_Table_Configuration(){
    aws ec2 create-route --route-table-id $PUB_MySQL_RT --destination-cidr-block 0.0.0.0/0 --gateway-id $IGW_ID --region $region
    aws ec2 associate-route-table  --subnet-id ${Sub_Pub_ID}  --route-table-id $PUB_MySQL_RT --region $region
    echo "Rules added in Public Route Table"
}

function Private_Route_Table_Configuration(){
    aws ec2 associate-route-table  --subnet-id ${Sub_PRI-1_ID} --route-table-id $PRI_MySQL_RT --region $region
    aws ec2 associate-route-table  --subnet-id ${Sub_PRI-2_ID} --route-table-id $PRI_MySQL_RT --region $region
    aws ec2 associate-route-table  --subnet-id ${Sub_PRI-3_ID} --route-table-id $PRI_MySQL_RT --region $region
    aws ec2 create-route --route-table-id $PRI_MySQL_RT --destination-cidr-block 0.0.0.0/0 --gateway-id $NAT_Gateway_ID --region $region
    echo "Rules added in Private Route Table"
}

function Security_Group(){
    # PUBLIC SECURITY GROUP
    SG_PUB=$(aws ec2 create-security-group --group-name MySQL_SG_PUB --description "For Bashion Host" --vpc-id $$ --region $region --query GroupId | tr -d '"')
    echo "Public_Security_Group_ID : ${SG_PUB}" >> Last_Build_detail
    aws ec2 authorize-security-group-ingress --group-id $SG_PUB --protocol tcp --port 22 --cidr 0.0.0.0/0 --region $region
    aws ec2 authorize-security-group-ingress --group-id $SG_PUB --protocol tcp --port 80 --cidr 0.0.0.0/0 --region $region
    echo "PUBLIC SECURITY GROUP created : ${SG_PUB} "

    # PRIVATE SECURITY GROUP
    SG_PRI=$(aws ec2 create-security-group --group-name MySQL_SG_PRI --description "For Bashion Host" --vpc-id $VPC_ID --region $region --query GroupId | tr -d '"')
    echo "Private_Security_Group_ID : ${SG_PRI}" >> Last_Build_detail
    aws ec2 authorize-security-group-ingress --group-id $SG_PRI --protocol tcp --port 22 --cidr 10.0.0.0/24 --region $region
    aws ec2 authorize-security-group-ingress --group-id $SG_PRI --protocol tcp --port 80 --cidr 0.0.0.0/0 --region $region
    echo "PRIVATE SECURITY GROUP created : ${SG_PRI} "
}

function Bastion_Host(){
    echo "Bastion host launching"
    Bastion_Host=$(aws ec2 run-instances --image-id $AMI --instance-type $TYPE --count $number --subnet-id ${Sub_Pub_ID}  --security-group-ids $SG_PUB --key-name $Key --region $region --query InstanceId | tr -d '"')
    echo " Bastion_Host : ${Bastion_Host}" >> Last_Build_detail
}
function Server_Launch(){
    server_1=$(aws ec2 run-instances --image-id $AMI --instance-type $TYPE --count $number --subnet-id ${Sub_PRI-1_ID}  --security-group-ids $SG_PRI --key-name $Key --region $region --query InstanceId | tr -d '"')
    server_2=$(aws ec2 run-instances --image-id $AMI --instance-type $TYPE --count $number --subnet-id ${Sub_PRI-2_ID}  --security-group-ids $SG_PRI --key-name $Key --region $region --query InstanceId | tr -d '"')
    server_3=$(aws ec2 run-instances --image-id $AMI --instance-type $TYPE --count $number --subnet-id ${Sub_PRI-3_ID}  --security-group-ids $SG_PRI --key-name $Key --region $region --query InstanceId | tr -d '"')

    echo " Server_MySQL_Main : ${server_1}" >> Last_Build_detail
    echo " Server_MySQL_Backup-1 : ${server_2}" >> Last_Build_detail
    echo " Server_MySQL_Backup-2 : ${server_3}" >> Last_Build_detail
}

function END(){
    echo "Infra for MySQL is successfully Created"
}