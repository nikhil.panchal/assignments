# ASSIGNMENT 6 : AWS
___________________________________

````
## What you will learn from this lab
- IAM(Authentication Management)
    - User Creation
    - Group Creation
    - Roles
    - Policies
## Resources Details
- EC2
- S3
- CloudWatch
- Route53
## Task Description
Create 3 Group's
- ### 1. Devloper
    - set of permissions:
        - ec2
            - start
            - stop
            - change instance type
            - read
        - S3
            - create
            - list
            - get
            - put
        - CloudWatch
            - read only
        - Route53
            - read only
- ### 2. DevOps
    - set of permissions:
        - DevOps should be able to manage all resources along with permission management.
- ### 3. QA:
    - set of permissions:
        - S3
            - list
            -  get
        - EC2
            - read only
        - Cloudwatch
            - read only
*create 3 user for your ninja buddy ,cross-buddy,code-reviewer and assign them one Group to each and verify permissions*
- Create a role for EC2 which will have permissions for
    - S3
        - list
        -  get
        - put
        - create
- Now create a EC2 and attach the created role to it
- Using aws-cli create a s3 bucket  using EC2 machine and try listing s3 bucket
````

# Task-1 : Create 3 group 

1. Devloper
2. DevOps
3. QA

## Step-1 : Creating Devloper group and 

1. to create gorup first we need to go to IAM service.
    1. search "IAM"
    2. open IAM service.
    ![](photo/IAM.png)

2. left side in dashboard you will see user Group tab -> go to user group

    ![](photo/User_group.png)

3. now click on create group

    ![](photo/click_on_create1_group.png)

4. give group name "Developer"
    ![](photo/Group_name_developer.png)

5. now we will attach policy to give permission on group and we will not use default policy and create custom policy for group

    ![](photo/permission_policy.png)

    ## Steps to create policy

    1. here is condition we need to fulfil/
    ````
    - ec2
            - start
            - stop
            - change instance type
            - read
    ````
    2. Go to Create Policy

        ![](photo/permission_policy.png)

    3. select service

        ![](photo/Select_service_ec2.png)

    4. select action

        ![](photo/Select_action_ec2.png)

    5. select resource

        ![](photo/Select_Resource_ec2.png)

    6. Review and click on create.

        ![](photo/Review_Ec2.png)

    as we have created Policy for Developer for ec2 we will create following policy.
    ````
    Developer :
    1. S3 = -create -list -get -put
    2. CloudWatch = -read only
    3. Route53 = -read only
    ````
    ![](photo/policy_developer.png)

6. now for developer group all policy is ready 

    ![](photo/attach_policy_developer.png)

7. so Developer Group is ready to use now.



## Step-2 : 

1. now we will create other two group and all policy we need for group

2. create policy for both group.

    `````
    DevOps :
    1. DevOps should be able to manage all resources along with permission management.
    `````
    ![](photo/policy_DevOps.png)


    `````
    QA:
    1. S3 = -list -get
    2. EC2 = -read only
    3. Cloudwatch = -read only
    ````
    ![](photo/policy_QA.png)+

3. all group are created

    ![](photo/Group.png)

## Step-3 : *create 3 user for your ninja buddy ,cross-buddy,code-reviewer and assign them one Group to each and verify permissions*

1. go to users -> Add Users

    ![](photo/User_create.png)

2. Create user Buddy : Mrituyanjay and added him to developer group.

    ![](photo/Buddy_Group.png.png)

3. Create user code-reviewer : Deepak and added him to QA group.

    ![](photo/Deepak.png)

4. Create user cross-buddy : Alok and added him to DevOps group.

    ![](photo/Alok.png)


# RESULT

1. Mrituyanjay can start stop ec2 but not able to create ec2 instance.

    1. Start instance

        ![](photo/Start_ec2_M.png)

    2. Stop instance

        ![](photo/Stop_ec2_M.png)

    3. launch instance

        ![](photo/Launch_ec2.png)




# TASK-2

````
- Create a role for EC2 which will have permissions for
    - S3
        - list
        -  get
        - put
        - create
- Now create a EC2 and attach the created role to it
- Using aws-cli create a s3 bucket  using EC2 machine and try listing s3 bucket
````


## Step-1 : Create a role for EC2

1. Create Policy for s3 : for this we will use Developer s3 rule as both are same policy.
2. Create Role

    ![](photo/Task2-step1.png)

3. attach this policy to role.

    ![](photo/Task2-step2.png)

4. click on create.

    ![](photo/Task2-step3.png)

5. Role Created.

    ![](photo/S3_Role.png)


## Step-2 : Create insatnce and add role to it

1. Launch instance.

2. In step 3 there is option IAM Role where we can define role for instance.

    ![](photo/Role_attach_ec2.png)

3. Instance Created.

    ![](photo/Instance_created.png)

4. now SSH to instance.

    ![](photo/SSH_to_intance.png)

# RESULT

1. Create S3 Bucket

    ![](photo/Create_s3.png)

2. List S3 Bucket

    ![](photo/ls_s3_bucket.png)




# AUTHOR
[NIKHIL.PANCHAL]
