___________________________________
# ASSIGNMENT 2 : AWS
____________________________________
````
DAY 2
Prerequisite
This is an addon in DAY 1 so it is must that you have DAY 1 Lab up and running to perform this lab
What you will learn from this lab

EC2-instance
Security Groups
Network access control list(NACL)
Load Balancers
Application Load Balancer(ALB) with Target groups
Network Load Balancer(NLB) with Target groups

Task

Create a bastion host in public subnet ninja-ec2-pub-01
port 22 of bastion host should only accessible from your public ip.
Install nginx in ninja-ec2-priv-01 hosted in private subnet
port 22 of the nginx server should only be accessible from  bastion host.
Create  Load Balancer
Application Load Balancer with target group as ec2 instance ninja-ec2-priv-01
hit the public dns arn created in google chrome and see if the nginx home page coming or not.
Network Load Balancer with Target group as ec2 instance ninja-ec2-priv-01
hit the public dns arn created in google chrome and see if the nginx home page coming or not.
Block port 22 using NACL of both private subnet and try accessing the private instances using ssh.
Observe the difference between Application Load balancer and Network Load Balancer.
Observe  the differences Between Security Groups and NACL & mention in your own words.
Instructions to perform this lab
Perform all the steps
Once you have performed all the steps  then make a Readme Doc with step by step representation of all the things you have done and attach screenshot in each step with explaination.
Good to Do
Perform all the steps through AWS CLI.
````

# Task
## ```step-1```
### Create a bastion host in public subnet 
- ninja-ec2-pub-01

![](photo/(1)bastion_host_created.png)

## ```step-2```
### port 22 of bastion host should only accessible from your public ip.
![](photo/(2)sg_only_22_bastion.png)

## ```step-3```
### Install nginx in ninja-ec2-priv-01 hosted in private subnet
![](photo/(3)nginx_installed_on_private_instance.png.png)

## ```step-4```
### port 22 of the nginx server should only be accessible from  bastion host.
![](photo/(4)port_22_for_nginx.png)

## ```step-5```
### Create  Load Balancer
### Application Load Balancer with target group as ec2 instance ninja-ec2-priv-01

#### Subnet configuration
![](photo/(5)a_alb_created_snap1_for_subnet_configuration.png)

#### ALB Security group details
![](photo/(5)b_alb_created_snap2_for_security_group.png)
![](photo/(5)c_alb_security_group.png)

#### ALB Target group information
![](photo/(5)d_alb_target_group_details_section.png)

![](photo/(5)e_alb_target_group_targets_information.png)

## ```step-6```
### hit the public dns arn created in google chrome and see if the nginx home page coming or not.
![](photo/(6)alb_dns_nginx_page_on_web.png)

## ```step-7```
### Network Load Balancer with Target group as ec2 instance ninja-ec2-priv-01

#### Basic Configuration
![](photo/(7)a_nlb_basic_configuration.png)

#### NLB Listner added
![](photo/(7)b_nlb_listener.png)

#### NLB Target group
![](photo/(7)c_nlb_target_healthy.png)

## ```step-8```
### hit the public dns arn created in google chrome and see if the nginx home page coming or not.
- But You will see that you are not able to access web page so we will configure NACL to move forward

## ```step-9```
### Block port 22 using NACL of both private subnet and try accessing the private instances using ssh.
![](photo/(9)NACL_created.png)
![](photo/(10)NACL_priv_subnet_associated.png)
![](photo/(11)NACL_inbound_rules_added.png)
![](photo/(11)NACL_ssh_blocked_for_private_subnet.png)






## AUTHOR
[NIKHIL.PANCHAL]