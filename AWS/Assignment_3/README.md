___________________________________
# ASSIGNMENT 3 : AWS
____________________________________
````
DAY 3

What you will learn from this lab

Load Balancer
Listener rules
target groups
security groups

Task

Setup LAMP stack in aws cloud & there should be 1 ec2 in each private subnet .

1 bastion host in public subnet .
port 22 of bastion host should only accessible from your public ip.
lamp 1 welcome page with path '/ninja1' on first ec2 should display        Welcome ninja-1.
lamp 2 welcome page with path '/ninja2' on second ec2 should display       Welcome ninja-2.
port 22 of  both the lamp servers should only be accessible from  bastion host.


Create target group for each lamp stack.
Create Application load balancer.

port 80 open of lamp stacks should only be accessible from your ALB.
port 80 open of ALB should only be accessible from your public IP.
ALB should only be accessible though your public IP none else.
create listener rule so that {YOUR-ALB-DNS-NAME}/ninja1 should display welcome page of first ec2.
create listener rule so that {YOUR-ALB-DNS-NAME}/ninja2 should display welcome page of second ec2.

Instructions to perform this lab

Perform all the steps
Once you have performed all the steps  then make a Readme Doc with step by step representation of all the things you have done and attach screenshot in each step with explaination.

Good to Do

Perform all the steps through AWS CL

````

# Task
## ```step-1```
### Setup LAMP stack in aws cloud & there should be 1 ec2 in each private subnet .
- lamp1(ninja-ec2-priv-01)
- lamp2(ninja-ec2-priv-02)

![](photo/(1)lamp_1_2_created.png)

#### Lamp 1(on ninja-ec2-priv-01)
![](photo/(1)a_lamp1_status.png)

#### Lamp 2(on ninja-ec2-priv-02
![](photo/(1)b_lamp2_status.png)

## ```step-2```
### 1 bastion host in public subnet  and port 22 of bastion host should only accessible from your public ip.
![](photo/(2)a_ssh_bastion_host_from_public_host.png)

![](photo/(2)b_port_22_for_ssh.png)

## ```step-3```
### lamp 1 welcome page with path '/ninja1' on first ec2 should display        
- Welcome ninja-1.

#### Setup webpage for lamp1 which will display 'Welcome ninja-1'
![](photo/(3)a_lamp1_ninja_curl.png)

## ```step-4```
### lamp 2 welcome page with path '/ninja2' on second ec2 should display      
- Welcome ninja-2.

#### Setup webpage for lamp2 which will display 'Welcome ninja-2'
![](photo/(3)b_lamp2_ninja_curl.png)

## ```step-5```
### Create Application load balancer.
![](photo/(5)alb_created.png)

## ```step-6```
### Create target group for each lamp stack.
![](photo/(6)target_groups_created.png)

### ```step-7```
### port 80 open of lamp stacks should only be accessible from your ALB.
![](photo/(7)a_alb-tg-01_health.png)

![](photo/(7)b_alb-tg-02_health.png)

### ```step-8```
### port 80 open of ALB should only be accessible from your public IP.
![](photo/(8)alb_port_80_for_public_ip.png)

### ```step-9```
### ALB should only be accessible though your public IP none else.

### Now when I hit dns name of alb , it will show target group1 page at first and in the second refresh it will load another target group page

![](photo/(9)a_first_alb_dns_hit.png)

![](photo/(9)b_second_alb_dns_hit.png)


### ```step-10```
### create listener rule so that {YOUR-ALB-DNS-NAME}/ninja1 should display welcome page of first ec2.
![](photo/apcninja1.png)

### ```step-11```
### create listener rule so that {YOUR-ALB-DNS-NAME}/ninja2 should display welcome page of second ec2.
![](photo/apcninja2.png)