
# assignment : 6 - shared library

````
In this lab, we will create a shared library to implement Java CI:-
- Create a shared library that includes these stages
    - Code stability
    - Code quality
    - Code coverage
- Add a stage to publish the report for code quality and coverage
- Add a stage for workspace cleanup,
- The shared library should take branch name and git URL as the argument
- Instead of writing code in Jenkins pipeline box, keep the code in form of Jenkinsfile at VCS
- Also send email notification.
````

## Step:1 : Create a shared library

**to create shared library we will follow this steps.**

1. create global repository which we will link in shared library for this case we can use gitlab or github.
in my case i am using gitlab repository for shared library.

![](photo/Gitlab_repo.png)

2. copy Http link of Repository.

![](photo/clone_repo.png)

3. open jenkins
4. go to manage jenkins -> configure system

![](photo/configure.png)

5. now look for "Global Pipeline Libraries" section and select add.
    - here we will provide library name.
    - specify branch
    - and last we will select retrival method : here we define how we will collect imformation from our global repository.
    - for this i have selected SCM method.
    - in SCM method we will give our global repository HTTP link.
    - if repository is private than we have to define its credential in it.
    - after this we will appy and save the setting.
    ` so our shared library is created.

![](photo/shared_library.png)
![](photo/shared_library_git.png)

6. now our library is created we will creat some function which we will call from library.
    1. mvn function
        1. name -> mavenjob.groovy
        ![](photo/mvnjob.png)
        2. name -> coberturaReport.groovy
        ![](photo/cobertura_report.png)

## Step-2 : creating script which add this conditions.

1. Create a shared library that includes these stages
    1. Code stability
    2. Code quality
    3. Code coverage
2. Add a stage to publish the report for code quality and coverage
3. Add a stage for workspace cleanup,
4. The shared library should take branch name and git URL as the argument

![](photo/script.png)


5. Also send email notification.

![](photo/email.png)

## Step-3 : creat pipeline job 

1. go to new iteam

2. select pipeline and give name assignment_6
    ![](photo/create_pipline.png)

3. Instead of writing code in Jenkins pipeline box, keep the code in form of Jenkinsfile at VCS

![](photo/PIpeline.png)

4. Run BUILD.


# **RESULT**

## BUILD : success
![](photo/result.png)

## Email 
![](photo/email_result.png)




## AUTHOR
[NIKHIL.PANCHAL]





