# ASSIGNMENT 5 : Jenkins
____________________________________

````
POINT : 1

- Authentication and Authorization
Their is an organization which has 3 teams
    - Developer
    - Devops
    - Testing
First you need to create 9 dummy jenkins jobs.Each job will print their jobname, build number.
    For Developer create 3 dummy jobs.In developer view
        job1:- dev-1
        job2:- dev-2
        job3:- dev-3
    For Testing create 3 dummy jobs. In testing view
        job1:- test-1
        job2:- test-2
        job3:- test-3
    For Devops create 3 dummy jobs. In devops view
        job1:- devops-1
        job2:- devops-2
        job3:- devops-3
Users in each team: 
    developer: [ They can see only dev jobs, can build it, see workspace and configure it ]
        - developer-1 
        - developer-2 
    testing: [ They can see all test jobs ,can build it, see workspace and can configure it, | They can also view dev jobs ]
        - testing-1 
        - testing-2 
    devops:  [ They can see all devops jobs ,can build it, see workspace and can configure it, | They can also view dev and test jobs  ]
        - devops-1 
        - devops-2
    admin
        -  admin-1 [ It will have full access ]
See what Authorization strategy suits it and implement it.
Also go through all authorization strategy
Legacy mode
Project Based
Matrix Based
Role-Based


POINT 2:-

Enable SSO with Goggle
````

# POINT : 1
--------------------------------------------------


# Step:1 : Creating view and adding jobs in it.

creating 3 views.
1. Developer
2. Testing
3. Devops

we can create view by clicking on new view option in left side panel.

**Steps to create View**

1. add name of view
2. select which job we need to see in this view

![](photo/view_created.png)

now we will add jobs to every view we have created.
lets see how we create job for this we will create freestyle job.

**steps to create job**

1. go to New item
2. Enter name for job
3. select job type for us it will be free style
4. click ok to create

![](photo/create_job.png)

so we had created job now our job must do this "Each job will print their jobname, build number."
so i will go to build part add execute shell then write this command.

![](photo/job_build.png)

so our first job is created for developer.

**Note:**
same we will create total 9 jobs as 3 jobs for each view.

![](photo/total_job.png)

# Step:2 : Creating user

as we have three team like developer, tester, & devops.
we will create two user per team.
like developer : developer-1 , developer-2

**Steps for Creating User**

1. Go to "manage Jenkins"
2. here you will see Security section. go to "Manage User"
3. on left side panel you will see "Create User" option click on it. now it will ask following detail.
    - username : create your username
    - password : add your password
    - Full Name : give Full name of user
    - Email : give email id

![](photo/Create_user.png)

4. click on "create User" and user will created.

**Note :** for this assignment i will create total 6 users.

![](photo/Users.png)


# Step:3 : assigning permission to user.

for this step we are using role-Based method to assign permission to all users.
to use this permission first we need to download a plug-in called "Role-based Authorisation Strategy"

## step-1
**steps to add plug-in**
1. go to Manage Jenkins -> Plugin Manager
2. search in available plug in "Role-based Authorization Strategy"
3. install.

## step-2
now we will select Roll base method in security part
1. go to Manage Jenkins -> Configure Global Security
2. in Authorization we will select Role-Based Strategy

![](photo/Role_Base_method.png)

3. apply and save.

## step-3
now we will create Roles.
1. go to Manage Jenkins -> Manage and Assign Roles
2. go to manage Roles
    - here we will create roles and assign them permission depending on working environment.

![](photo/role_created.png)

## step-4
1. after creating Role we will assign this Role to its User so that User have permission related to its Role.
2. User : Developer
    - developer: [ They can see only dev jobs, can build it, see workspace and configure it ]

![](photo/Developer_Role.png)

3. User : Tester
    - testing: [ They can see all test jobs ,can build it, see workspace and can configure it, | They can also view dev jobs ]

![](photo/Tester_Role.png)

4. User : Devops
    - devops:  [ They can see all devops jobs ,can build it, see workspace and can configure it, | They can also view dev and test jobs  ]

![](photo/Devops_Role.png)

5. User : Admin
    -  admin-1 [ It will have full access ]

![](photo/admin_role.png)

## step-5
now we have assign all job specific permission to User but if we try to login as user we will not able to see jenkins as we have not specified globel role for all user so now we will do this.

1. adding team role of overall view for Jenkins.

![](photo/Team_for%20read.png)

2. now adding this role to all User.

![](photo/Global_Role_assign.png)


**NOTE :**
ALL THE STATEMENT PROBLEM IS RESOLVED SO NOW WE WILL SEE THE RESULT.


# 1. ADMIN VIEW.
in admin view we can see we have all access to jobs and all the functions of jenkins.
![](photo/admin_View.png)

# 2. DEVELOPER VIEW.
in Developer view we can see we developer view so we can only see developer jobs.

![](photo/Developer_View.png)

1. if we look in jobs we can only build,config and see workspace of the dev job.
we are not able to delete this job.

![](photo/Dev_dev_job.png)


# 3. TESTER VIEW.
In Tester view we can see both Developer and Tester Job.

![](photo/Tester_view.png)

1. if we look in Test jobs we can only build,config and see workspace of the dev job.
we are not able to delete this job.

![](photo/Tester_test_job.png)

2. if we go to dev job we can only read this job.

![](photo/Tester_dev_job.png)

# 4. DEVOPS VIEW.
In Tester view we can see both Developer and Tester Job.

![](photo/Devops_view.png)

1. if we look in Devops jobs we can only build,config and see workspace of the dev job.
we are not able to delete this job.

![](photo/Devops_Devops_job.png)

2. if we go to dev job we can only read this job.

![](photo/Devops_dev_job.png)

3. if we go to Test job we can only read this job.

![](photo/Devops_tets_job.png)




# POINT:2
-------------------------
Enable SSO with Goggle







## Author
[@nikhil.panchal]S
