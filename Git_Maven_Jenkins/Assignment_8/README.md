___________________________________
# ASSIGNMENT 8
____________________________________
````
In this lab, we will create a dsl multibranch pipeline job to implement Java CI:-
- Create a shared library that includes these stages
    - Code stability
    - Code quality
    - Code coverage
- Add a stage to publish the report for code quality and coverage
- Add a stage for workspace cleanup,
- The shared library should take branch name and git URL as the argument
- Instead of writing code in Jenkins pipeline box, keep the code in form of Jenkinsfile at VCS
````
## Step-1 : Creating Git repo with multiple branch.

1. i have created multiple branch and also added jenkins file in branches.

![](photo/git_branch.png)

## Step-2 : editing jenkins file which have this conditions.

1. Create a shared library that includes these stages
    1. Code stability
    2. Code quality
    3. Code coverage
2. Add a stage to publish the report for code quality and coverage
3. Add a stage for workspace cleanup,
4. The shared library should take branch name and git URL as the argument

![](photo/script.png)


5. Also send email notification.

![](photo/email.png)

## Step-3 : DSL Job

1. first we need to install DSL plug-in.
    1. Go to manage jenkins -> manage plugin
    2. click on available tab
    3. search "job DSL"
    4. install it.
![](photo/DSL_job.png)

2. now we have installed job DSL plugin so we have enabled the dsl option in build.
3. DSL job is always freestyle job.
4. go to new item 
5. give job name assigment_8 and select freestyle
![](photo/assignmetn_8.png)
6. in configuration go to build part.
7. select "process job DSL"
8. now we will write script for seed job.
![](photo/script.png)

9. now we will run the job.

## **RESULT**

1. Build success : so new pipline job is created.

![](photo/result_8.png.png)

2. here is pic of DSL_assignmnet job which is created by seed job

![](photo/result_9.png)





## AUTHOR
[NIKHIL.PANCHAL]
