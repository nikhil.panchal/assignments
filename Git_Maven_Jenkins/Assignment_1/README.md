___________________________________
# ASSIGNMENT 1 : GIT
____________________________________
```
Must Do

- Create a folder ninja at the root level of your cloned code
- Add a file README.md with content "Trying fast forward merge"
- Create a branch ninja and move to it
- Run git status command
- Commit your changes to ninja branch
- Merge ninja branch to master branch make sure that a new commit get's created
- Assuming you are in master branch, modify README.md with content Changes in master branch, commit the changes in master branch.
- Switch to ninja branch, modify README.md with content Changes in ninja branch, commit the changes in ninja branch.
- Merge master branch to ninja branch in such a fashion that changes of master branch overrides changes in ninja branch
- Revert the above merge commit

```
## 1. Create a folder ninja at the root level of your cloned code

**Step - 1**

create folder/directory named ninja at root level
.md
```
$ mkdir ninja
```

![](photo/make_ninja_directory.png)

**step - 2**

after creating directory use clone command to clone gitlab directory by adding clone link

```
$ git clone <Gitlab clone link>
```
this will create linked repository in the current working directory.

>example is give below.

![](photo/Git_Clone.png)



## 2. Add a file README.md with content "Trying fast forward merge"

so after cloning repository all the content will be available in directory.

now open text editor and open README file.
here i am using vim text editors.

```
$ vim README.md
```
add text "Trying fast forward merge" into the README.md file.

![](photo/add_file.png)

## 3.  Create a branch ninja and move to it
**Step - 1**
first we check which branch we are in and what branch we have.
```
$ git branch
```
![](photo/Branch_check.png)

so as you can see in photo here we are currently in main branch and we only have one branch so lets create one.

**Step - 2**
to create branch we will use below command
```
$ git branch {branch_name}
```
so lets create ninja branch.

![](photo/branch_create.png)

as in photos we have created ninja branch but we are still in main branch which represetd by * symbol

**step - 3**
to change branch we use checkout command first lets see in which branch we are wornking so by using below command we can chech our working branch
```
$ git checkout {branch_name}
```
so lets go to ninja branch.

![](photo/ninja_checkout.png)

## 4. Run git status command

here we will use this command
```
$ git status
```

![](photo/git_status.png)

## 5. Commit your changes to ninja branch

to commit changed we will first add file to staging area and after that we use commit command to commit the changes in local repo.

![](photo/git_commit.png)

## 6. Merge ninja branch to master branch make sure that a new commit get's created
to merge the branch we use merge command.
```
$ git merge {branch_name_which_we_want_to_merge}
```
![](photo/git_merge.png)

## 7. Assuming you are in master branch, modify README.md with content Changes in master branch, commit the changes in master branch.
modify README.md file.
commit the changes.

![](photo/main_branch_changes.png)


## 8. Switch to ninja branch, modify README.md with content Changes in ninja branch, commit the changes in ninja branch.

![](photo/ninja_branch_changes.png)

## 9. Merge master branch to ninja branch in such a fashion that changes of master branch overrides changes in ninja branch
**step-1**
first lets try to merge.

![](photo/ninja_merge.png)

as we can see when we tried to merge main branch to ninja branch both have commited changes on same file line so it created conflict which now we have to resolve it.
so when you u cat the file it will show us HEAD and main branch changes and we can take which we want as in this case we need to override changes of ninja branch to main branch.

![](photo/changes%20of%20master%20branch%20overrides%20changes%20in%20ninja%20branch.png)

## 10. Revert the above merge commit

so we have used revert command and given last commit hash to it and define which number ofcommit it need.

![](photo/Revert1.png)


## AUTHOR
[NIKHIL.PANCHAL]

