___________________________________
# ASSIGNMENT 4
____________________________________
````
Code Stability - mvn compile
Code Quality - mvn checkstyle:checkstyle
Implement Continuous Integration using Jenkins:-
Task -1
- Add a Jenkins step to perform code coverage using Cobertura
- Install the Cobertura plugin in Jenkins and publish a code coverage report on it. Report path **target/site/cobertura/coverage.xml
- Add a Jenkins step to generate war file artifact and get it is using archive artifact in post-build actions
 - Create a Jenkins Declarative Pipeline for CI  :

Task-2
Create a Jenkins Pipeline that will kick start the CI process if a commit is made.
Add a Stage in pipeline to perform to Code stability.
Add a Stage in pipeline to perform code quality analysis.
Add a Stage in pipeline to perform code coverage analysis.
Add a Stage in pipeline to generate report for code quality & analysis.
````
## step-1 : Code Stability - mvn compile
1. create job with pipeline style and add name assignment_4
2. go to build part and add pipline script
3. add Git clone stage
4. add mvn compile stage 

![](photo/mvn_compile.png)

## step-2 : Code Quality - mvn checkstyle:checkstyle
now add code quality stage in pipeline

![](photo/cod_quality.png)
# Task -1
1. Add a Jenkins step to perform code coverage using Cobertura

![](photo/Cobertura.png)

2. Install the Cobertura plugin in Jenkins and publish a code coverage report on it. 
    1. go to manage jenkins -> Manage Plug-in
    2. click on available and search "Cobertura Plugin"
    3. install "Cobertura Plugin"
    ![](photo/plig-in.png)

3. Report path **target/site/cobertura/coverage.xml
    for report we will add report step in coverage stage.
    ![](photo/report.png)

4. Add a Jenkins step to generate war file artifact and get it is using archive artifact in post-build actions
    1. for artifact we will add this in post block.
    2. and create alwasy block for this.
    ![](photo/atrifact.png)

# RESULT 
Create a Jenkins Declarative Pipeline for CI

![](photo/result.png)


# TASK-2

Create a Jenkins Pipeline that will kick start the CI process if a commit is made.
Add a Stage in pipeline to perform to Code stability.
Add a Stage in pipeline to perform code quality analysis.
Add a Stage in pipeline to perform code coverage analysis.
Add a Stage in pipeline to generate report for code quality & analysis.

**NOTE:** for this we will use "Build when a change is pushed to GitLab" which will allow jenkins job to start job if any commit is made in Git repp.

![](photo/task-2.png)

so now when we commited in our git repo this job started automatically.

![](photo/result2.png)



## AUTHOR
[NIKHIL.PANCHAL]


