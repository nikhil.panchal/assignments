___________________________________
# ASSIGNMENT 7
____________________________________
````
In this lab, we will create a multibranch pipeline to implement Java CI:-
- Create a shared library that includes these stages
    - Code stability
    - Code quality
    - Code coverage
- Add a stage to publish the report for code quality and coverage
- Add a stage for workspace cleanup,
- The shared library should take branch name and git URL as the argument
- Instead of writing code in Jenkins pipeline box, keep the code in form of Jenkinsfile at VCS
- Send email and slack notification.
````

## Step-1 : Creating Git repo with multiple branch.

1. i have created multiple branch and also added jenkins file in each branches.

![](photo/git_branch.png)

## Step-2 : editing jenkins file which have this conditions.

1. Create a shared library that includes these stages
    1. Code stability
    2. Code quality
    3. Code coverage
2. Add a stage to publish the report for code quality and coverage
3. Add a stage for workspace cleanup,
4. The shared library should take branch name and git URL as the argument

![](photo/script.png)


5. Also send email notification.

![](photo/email.png)

## Step-3 : Create Multi-Pipeline JOb.

1. Go to New Item
2. select multi-pipeline and give name assignment_7

![](photo/multipipeline.png)

3. Configue Job.
    1. we will provide git repo in which we need to run multipipeline job.
    ![](photo/branch_multi.png)

# RESULT

1. As we have created total 5 branch in our git repo.
2. multi pipeline job will create 5 job.

![](photo/result.png)

3. Branch Result : master

![](photo/master.png)


**Note:** multipipeline job only create branch job which have jenkins file which we has define in configuration. 

## AUTHOR
[NIKHIL.PANCHAL]
