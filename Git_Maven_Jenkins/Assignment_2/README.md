___________________________________
# ASSIGNMENT 2
____________________________________

```
Enable a functionality in your local repository to not allow a commit if it does not follow the below condition.
    Example:-
        git commit -m "[JIRA-xxxx]:status=done"
        git commit -m "[JIRA-xxxx]:status=inprogress"

Commit your Last assignment shell script into your GitLab repo. then use git plugin to execute the shell script:-
    - Which take input as a string parameter, then publish your HTML report.
    - Send mail Notification on the successful execution of the code. the notification shall contain an HTML report URL.
```

# TASK : 1
Enable a functionality in your local repository to not allow a commit if it does not follow the below condition.

Example:-

        git commit -m "[JIRA-xxxx]:status=done"


# Steps:

## Step-1

go to Git repo -> cd to .git/
![](photo/task1.1.png) 

## Step-2

now look for hooks directory and cd into it and search for commit-msg script.
![](photo/task1.2.png) 

## Step-3

Vim to pre-commit and check the script and edit it according to your use.
![](photo/task1.3.png)

## Step-4
chnage the file name from pre-commit.sample to pre-commit
![](photo/task1.4.png)




# TASK : 2
Commit your Last assignment shell script into your GitLab repo. then use git plugin to execute the shell script:-
1. Which take input as a string parameter, then publish your HTML report.
2. Send mail Notification on the successful execution of the code. the notification shall contain an HTML report URL.

## STEPS :

1. here is the my shell script in gitlab repo.
![](photo/linux.png)

2. create freestyle job with name assignment_2
![](photo/job.png) 

3. Which take input as a string parameter
    1. for function i am using choice parameter.
    ![](photo/choice_para.png)

    2. for other input i am using string parameters
    ![](photo/string.png)

4. Build script
![](photo/build.png) 

5. public HTML Report
![](photo/HTML.png) 

6. email notification
![](photo/email.png)


**RESULT**

1. INPUT method
![](photo/input.png)

2. Build Output
![](photo/output.png)

3. email notification
![](photo/email_Result.png)

## AUTHOR
[NIKHIL.PANCHAL]
