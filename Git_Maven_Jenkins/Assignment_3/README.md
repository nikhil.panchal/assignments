___________________________________
# ASSIGNMENT 3
____________________________________
```
Create a jenkins job in which we can pass the days as a parameter so that it can perform these operations:-
	- Clone the repo
    - It should generate a terminal output of Author Name, Author Email, Commit ID, File Name, Date and make sure it should fetch the details for respective days which we have given as input.
    - Add HTML output capability to the Jenkins to generate an HTML report.
- Create another shell script that will take a repository as input and check if there are any changes in the repo or not. In case of changes, it should write the diff to a file in /tmp/<repo_name>. Make sure this jenkins job can run periodically to fetch the repository information.
```

# TASK: 1 : Create jenkins job

1. Go to new iteam tab
2. select freestyle job and add name for job.
**note:**here i am giving assignment_3 name to job.

![](photo/job_type.png)

3. here we need and parameter which ask for days.
for this we will use string parameter

![](photo/parameters.png)

4. clone the repo
for this we will add git repo to source code management portion.

![](photo/git_clone.png)

5. It should generate a terminal output of Author Name, Author Email, Commit ID, File Name, Date and make sure it should fetch the details for respective days which we have given as input.

for this we will create script in build portion and select execute shell 

![](photo/build.png)

6. Add HTML output capability to the Jenkins to generate an HTML report.

for this we will select post build action and there is option called "Publish HTML reports"

![](photo/html.png)

## RESULT

![](photo/Result1.png)
![](photo/reslut1.2.png)

# TASK : 2
```
Create another shell script that will take a repository as input and check if there are any changes in the repo or not. In case of changes, it should write the diff to a file in /tmp/<repo_name>. Make sure this jenkins job can run periodically to fetch the repository information.
```
1. create freestyle job as above.
here input will be git repo link and git repo name

![](photo/input_git_repo_link.png)
![](photo/input_git_repo_name.png)

2. we will create script using diff command to check differece between repo.

![](photo/script.png)

3. Make sure this jenkins job can run periodically to fetch the repository information.

for this we will go to BUILD Triggers and select "Build periodically"

![](photo/trigger.png)


## AUTHOR
[NIKHIL.PANCHAL]







