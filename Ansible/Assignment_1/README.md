# ASSIGNMENT 1 : Ansible
_________________________________
`````
Topic: Installing Ansible

Topics to be Covered

* Installing Ansible using pip, yum or apt
* Automating Ansible Installation
* Configuration file basics
* Ansible inventory Usage
* How to install a package
* What You Will Learn
* How easy it is to install and configure Ansible for yourself.

The Assignment
Use pip to install the ansible package and its dependencies to your control machine.
Display the Ansible version and man page to STDOUT.
Check all the possible parameters you need to know in ansible.cfg file.
Ansible Inventory: Check the default inventory file for ansible control master and use inventory, run ansible ping commands on various inventory groups. Try this on minimum of two virtual machines.(You can use any of these Docker/Vagrant)
In ~/.ansible.cfg file (create the file if it doesn't exist already) do the following:
Create a new directory ~/.ansible/retry-files and set retry_files_save_path to it.
Set the Ansible system forks to 10
Problem statement: Using Adhoc command
- Host a static website on minimum three hosts using inventory, content of static website is "Index page of Ansible assignment"
Document root /opt/html

`````

## TASK:1 - Use pip to install the ansible package and its dependencies to your control machine.

1. First SSH to master server where we want to install ansible.
    ![](photo/SSH_to_Master.png)

2. Ansible can be installed via “pip”, the Python package manager. If ‘pip’ isn’t already available in your version of Python, you can get pip by:

````
$ sudo apt update
$ sudo apt install python3-pip
````

![](photo/apt_update.png)
![](photo/pip_install.png)

3. installing ansible

    ![](photo/pip_install_ansible.png)

## TASK:2 - Display the Ansible version and man page to STDOUT.

1. We will use following command
```
$ ansible --version
```

2. RESULT :
    ![](photo/version.png)

## TASK:3 - Check all the possible parameters you need to know in ansible.cfg file.

1. go to /etc/ansible

    ![](photo/ansible.cnfg.png)

2. vim ansible.cfg

    ![](photo/view_ancible.cfn.png)

## TASK:4 - Ansible Inventory: Check the default inventory file for ansible control master and use inventory, run ansible ping commands on various inventory groups. Try this on minimum of two virtual machines.(You can use any of these Docker/Vagrant)

1. for default Inventory we can go to /etc/ansible/hosts

2. but here  we have created invetory file

    ![](photo/host.png)

3. ping all three host
    1. N1 :

        ![](photo/N1.png)

    2. N2 :

        ![](photo/N2.png)

    3. N3 :

        ![](photo/N3.png)


## TASK:5 - In ~/.ansible.cfg file (create the file if it doesn't exist already) do the following:
```
    - Create a new directory ~/.ansible/retry-files and set retry_files_save_path to it.
    - Set the Ansible system forks to 10
    - Problem statement: Using Adhoc command
    - Host a static website on minimum three hosts using inventory, content of static website is      "Index page of Ansible assignment"
    - Document root /opt/html
```
1. creating group of host

    ![](photo/group.png)

2. Create a new directory ~/.ansible/retry-files and set retry_files_save_path to it.

    1. Creat new directory

        ![](photo/retry_file.png)
    
    2. set retry_files_save_path to it.
        1. go to ansible.cnf file
        2. edit in config file 

        ![](photo/Retry_cnfg.png)

3. Set the Ansible system forks to 10

        ![](photo/fork.png)

4. Host a static website on minimum three hosts using inventory, content of static website is      "Index page of Ansible assignment"
    
    1. install nginx in all node
        ![](photo/prod_update.png) 
        ![](photo/nginx_install.png)

    2. Create Index.html file.
        ![](photo/indext.html.png)

    3. CP index.html file to all node
        ![](photo/cp_File.png)

    4. curl localhost
        ![](photo/curl.png)

## AUTHORE
[NIKHIL.PANCHAL]

