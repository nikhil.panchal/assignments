# ASSIGNMENT 3 : Ansible
_________________________________
`````
The Assignment
Create and delete ninja directory on host machine
Set hostname on all nodes from remote machine
Fetch os of all nodes and store o/p into a file, use min two different machine of different flavour of os.
Add three group into hosts file through ansible module.
          Debian ( ip of debian machine)
          Centos ( ip of centos machine)
          All ( ip of all nodes )
Problem statement: Using Adhoc command
Step 1:
    * Install apache on Debian machine
    * Cross check apache isntalled or not from remote machine
    * Apache runn  on 8082 port
    * Create two virtual host
    * Restart apache from remote machine
Step2:
   * Install nginx on centos machine
   * Nginx run on 8080 port.
Step3:
   * Configure Nginx - configure it to

`````

## Task:1 - Create and delete ninja directory on host machine

1. create ninja directory
        
    ![](photo/Mk_DIR.png)

2. Delete Ninja Directory

    ![](photo/DEL_DIR.png)

## RESULT

![](photo/DIR_Result.png)

## Task:2 - Set hostname on all nodes from remote machine

    ![](photo/hostname.png)

## RESULT

![](photo/result_hostname.png)


## Task:3 - Fetch os of all nodes and store o/p into a file, use min two different machine of different flavour of os.

    ![](photo/os1.png)

## RESULT

![](photo/os2.png)

## Task:4 - Add three group into hosts file through ansible module.

1. Debian ( ip of debian machine)

    ![](photo/host_debian.png)

2. Centos ( ip of centos machine)

    ![](photo/host_centos.png)

3. All ( ip of all nodes )

    ![](photo/host_all.png)


# Problem Statement

## Step 1 : 

1. Install apache on Debian machine

    ![](photo/Debian_nginx.png)

2. Cross check apache isntalled or not from remote machine

    ![](photo/apache_cnfrm1.png)
    ![](photo/apache_cnfrm2.png)

3. Apache runn  on 8082 port

    ![](photo/port_apache.png)

4. Create two virtual host

    ![](photo/VH_S1.png)
    ![](photo/VH_S2.png)
    ![](photo/VH_S3.png)
    ![](photo/VH_S4.png)
    ![](photo/VH_S5.png)
    ![](photo/VH_S6.png)    
    ![](photo/VH_S7.png)
    ![](photo/VH_S8.png)
    ![](photo/VH_S9.png)

5. Restart apache from remote machine

    ![](photo/restart_apache.png)

# RESULT



## Step2:

1. Install nginx on centos machine

    ![](photo/Centos_nginx.png)

2. Nginx run on 8080 port.

    ![](photo)

## RESULT