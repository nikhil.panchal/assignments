# ASSIGNMENT 2 : Ansible
_________________________________
`````
Perform the following operations using ad-hoc commands and ansible modules :

* Fetch and display to STDOUT Ansible facts using the setup module.
* Fetch and display only the "virtual" subset of facts for each host.
* Fetch and display the value of fully qualified domain name (FQDN) of each host from their Ansible facts.

* Display the uptime of all hosts using the command module.
* Ping all hosts except the 'control' host using the --limit option
* Setup Java8 on the hosts in the "App" group using the apt module.
* Setup and enable the EPEL package repository on the hosts in the "web" group using the yum module.

CentOS systems use the latest epel-release package
RHEL systems should use https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

* Create inventory groups app and web

* set a cron on ansible control machine that will run every 1 minute ,  
    *perform below tasks:-

    - execute ansible adhoc commands on client machines (cannot be control machine) , to  create a file in /var/log/ninja_name on all the client nodes,

    - system_hostname [:space:] system_time in the file every 1 minute using ansible facts.

`````
## TASK:1 - Create inventory groups app and web
    
![](photo/Group_created.png)

## TASK:2 - Fetch and display to STDOUT Ansible facts using the setup module.

![](photo/task2.png)

## TASK:3 - Fetch and display only the "virtual" subset of facts for each host.

![](photo/Task3.png)

## TASK:4 - Fetch and display the value of fully qualified domain name (FQDN) of each host from their Ansible facts.

![](photo/TASK4.png)

## TASK:5 - Display the uptime of all hosts using the command module.

![](photo/TASK5.png)

## TASK:6 - Ping all hosts except the 'control' host using the --limit option


## TASK:7 - Setup Java8 on the hosts in the "App" group using the apt module.

![](photo/JAVA8.png)

## TASK:8 - Setup and enable the EPEL package repository on the hosts in the "AMZ" group using the yum module.

![](photo/EPEL.png)

## TASK:9 - set a cron on ansible control machine that will run every 1 minute

