# ASSIGNMENT 5 : Ansible
_________________________________
`````
1. Create an Ansible playbook to rotate SSH keys.
   	Explanation - Replacing the keys you're currently using with new keys, and removing the ability for old keys to be used to log into your systems.
		- Create a new key
		- Add new key to authorized_keys files on your nodes
		- Test new key
		- Remove previous keys from authorized_keys files on your nodes.
		- again test the connectivity with the new keys.

`````

## Task:1 - Create a new key



## Task:2 - Add new key to authorized_keys files on your nodes

## TaSK:3 - Test new key

## Task:4 - Remove previous keys from authorized_keys files on your nodes.

## Task:5 - again test the connectivity with the new keys.
