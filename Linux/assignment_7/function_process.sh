#!/bin/bash
#set -x



#./ otProcessManager topProcess 5 memory
#./ otProcessManager topProcess 10 cpu
function top_process()
{
    sum=`expr $1 + 7`
    number_of_line=$sum
    filter_by=$2

    case $filter_by in
        pid) top -o PID | head -$number_of_line;;
        user) top -o USER | head -$number_of_line;;
        ni) top -o NI | head -$number_of_line;;
        SHR) top -o SHR | head -$number_of_line;;
        memory) top -o %MEM | head -$number_of_line;;
        cpu) top -o %CPU | head -$number_of_line;;
        command) top -o COMMAND | head -$number_of_line;;
        time) top -o TIME+ | head -$number_of_line;;
        *) echo "please use one of the options : pid/user/ni/SHR/memory/cpu/command/time "
    esac
   
}


#./ otProcessManager killLeastPriorityProcess
function kill_least_priority_process()
{
    leastprocess=$(ps -eo pid,ni,cmd --sort=-ni | head -2 | tail -n 1 | awk '{print $1}')
    kill $leastprocess
}




#./ otProcessManager killProcess <processName>/<processID>
function kill_Process()
{
    
    process=$1
    if [[ $yournumber =~ $re ]]; 
        then
            kill $process
        else
            pkill $process
    fi
}

#./ otProcessManager ListWaitingProcess
function ListWaitingProcess(){}


#./ otProcessManager RunningDurationProcess <processName>/<processID>
function RunningDurationProcess(){}


#./ otProcessManager listOrphanProcess
function listOrphanProcess(){}


#./ otProcessManager listZoombieProcess
function listZoombieProcess(){
ps -eo pid,s,user,cmd | awk {' if ( $2 == "Z") print $0 '}
}


