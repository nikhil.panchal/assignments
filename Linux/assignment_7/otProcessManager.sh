#!/bin/bash
set -x

#1) Create a process management utility, to find
#- Top n process by memory
#- Top n process by cpu
#- Kill process having least priority 
#- running duration of a process by name or pid
#- List orphan process if any
#- List zoombie process if any
#- Kill process by name or pid
#- List process that are waiting for the resources
#```
#i.e.
#./ otProcessManager topProcess 5 memory
#./ otProcessManager topProcess 10 cpu
#./ otProcessManager killLeastPriorityProcess 
#./ otProcessManager RunningDurationProcess <processName>/<processID>
#./ otProcessManager listOrphanProcess
#./ otProcessManager listZoombieProcess
#./ otProcessManager killProcess <processName>/<processID>
#./ otProcessManager ListWaitingProcess

source function_process.sh

function=$1
shift
case $function in
    topProcess) top_process "$@";;
    killLeastPriorityProcess) kill_least_priority_process "$@";;
    RunningDurationProcess)RunningDurationProcess "$@";;
    listOrphanProcess)listOrphanProcess;;
    listZoombieProcess)listZoombieProcess;;
    killProcess)kill_Process "$@";;
    ListWaitingProcess)ListWaitingProcess;;
    --help) user_help;;
    *) echo " wrong input : please type --help for more help"

esac


