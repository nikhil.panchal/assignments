#!/bin/bash
#set -x
#Create a shell script to generate a star, it will take 2 arguments size & type
#total 7 type

source style_function.sh 
style=$2
size=$1
if [["$size" = "help" ]];
    then
        user_help
else
    case $style in
        t1) t1;;
        t2) t2;;
        t3) t3;;
        t4) t4;;
        t5) t5;;
        t6) t6;;
        t7) t7;;
        *) echo " wrong input add : try --help to get more detail "
    esac
fi
