#!/bin/bash
#set -x

#Create a shell script that will take take a number and print below output depending on the conditions
    #tom -> If number is divisible by 3
    #cat -> If number is divisible by 5
    #tomcat -> If number is divisible by 15


num=$1

if (( $num % 15 == 0 ))
    then
        echo " tomcat "

    elif (( $num % 3 == 0 ))
    then
        echo " tom "

    elif (( $num % 5 == 0 ))
    then
        echo " cat "
    else
        echo " number is no divisible by 3 or 5 "
fi
