# Assignment-4
# Star Pattern and Tomcat Utility
​
This Utility will be able to do below operations:
​
| Utility             | Description                                                               |
| ----------------- | ------------------------------------------------------------------ |
| drawStar | This will generate a star, it will take 2 arguments size & type                   |
| printTomcat | This will take take a number and print below output depending on the conditions  |
​
​




# task-1 : drawstar​

## here is the example of how to run drawstar function.
​
1. To generate a t1 type start with size 5

```​
./drawStar.sh 5 t1
    *
   **
  ***
 ****
*****
```

2. To generate a t2 type start with size 5

```
./drawStar.sh 5 t2
*
**
***
****
*****
```

3. To generate a t3 type start with size 5
```
./drawStar.sh 5 t3
    *
   ***
  *****
 *******
*********
```
4. To generate a t4 type start with size 5
```
./drawStar.sh 5 t4
*****
****
***
**
*
```
5. To generate a t5 type start with size 5
```
./drawStar.sh 5 t5
*****
 ****
  ***
   **
    *
```
6. To generate a t6 type start with size 5
```
./drawStar.sh 5 t6
*********
 *******
  *****
   ***
    *
```

```
7. To generate a t7 type start with size 5
```
bash
$ ./drawStar.sh 5 t7
      *
     * *
    * * *
   * * * *
  * * * * *
   * * * *
    * * *
     * *
      *
```


# task-2 : printTomcat

Run the script(for print any word i.e tom, cat and tomcat)
```bash
    ./printTomcat.sh [Number]
```

## Example

```
To print conditional string i.e tom, cat and tomcat
​
```bash
$ ./printTomcat.sh 18
tom
$ ./printTomcat.sh 10
cat
$ ./printTomcat.sh 60
tomcat

```
​
## Authors
- [@nikhil.panchal]