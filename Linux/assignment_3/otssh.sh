#!/bin/bash
source sshfunction.sh
OPERATION=""
#PORT="22"

while getopts alduch:n:U:i:p: options; 
do
case $options in
    a) OPERATION="add" ;;
    h) HOSTNAME=$OPTARG ;;
    n) SERVERNAME=$OPTARG ;;
    U) USERNAME=$OPTARG ;;
    i) KEYPATH=$OPTARG ;;
    p) PORT=$OPTARG ;;
    l) OPERATION="list" ;;
    d) OPERATION="delete" ;;
    u) OPERATION="update" ;;
    c) OPERATION="connect";;
    esac
done

case $OPERATION in 
        add) addSSHConnectionDetails "$SERVERNAME" "$HOSTNAME" "$USERNAME" "$PORT" "$KEYPATH";;
       list) listSSH ;; 
     update) updateSSH $SERVERNAME ;;
     delete) deleteSSHConnection $SERVERNAME ;; 
    connect) connectSSH $SERVERNAME ;;
          *) echo "please select a valid operations -a ----> add 
                                 -u ----> update
                                 -d ----> delete
                                 -c ----> connect
                                 -l ----> list" ;;
    esac



