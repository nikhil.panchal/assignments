# assignment-3
# otssh Utility
​
This utility will perform the below operations:
​
​
## Operations
​
|    Operations |  Description |  
| ----------------- | ---------------------------------|
| ./otssh.sh -a -n [HostName] -h [HostIP] -u [UserName] -p 2022 -i [KeyPath] | Add a new ssh connection |
| ./otssh.sh ls | To list all ssh connections with Hostname only |
| ./otssh.sh ls -d | To list all ssh connections with full details |
| ./otssh.sh -u | To update ssh connection with given details |
| ./otssh.sh rm | To remove a ssh connection |
| ./otssh.sh | To connect to a server mention server |
​
​
## Steps to execute utility
​
1. To add a new ssh connection
​
```
./otssh.sh -a -n [HostName] -h [HostIP] -u [UserName] -p 2022 -i [KeyPath]
```
​
2. To list all ssh connections with Hostname only
```
./otssh.sh ls
```
​
3. To list all ssh connections with full details
```
./otssh.sh ls -d
```
4. To update ssh connection with given details
```
./otssh.sh -u [Enter Details To Be Updated]
```
​
5. To remove a ssh connection
```
./otssh.sh rm [ServerName]
```
​
6. To connect to a server mention server
```
./otssh.sh [ServerName]
```
​
​
​
​
## Authors
​
- [@nikhil.panchal]