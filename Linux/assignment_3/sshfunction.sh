#!/bin/bash
function serverDetails() {
    ALIAS=$1
    SSH_CONNECTION_STRING=$2
    echo $SSH_CONNECTION_STRING
        echo "${SSH_CONNECTION_STRING}" > ./ssh_connections/${ALIAS}.sh
        chmod +x ./ssh_connections/${ALIAS}.sh
}

function addSSHConnectionDetails () {
    ALIAS=$1
    HOSTNAME=$2
    USERNAME=$3
    PORT=${4:-\2\2}
    KEY_PATH=${5:-~/.ssh/id_rsa.pub}
    SSH_CONNECTION_STRING="ssh -p $PORT -i $KEY_PATH $USERNAME@$HOSTNAME"

    serverDetails "$ALIAS" "$SSH_CONNECTION_STRING"

}

function connectSSH () {

    ALIAS=$1
    ./ssh_connections/${ALIAS}.sh

}

function listSSH () {

    ls -l ./ssh_connections/ | awk '{print $9}'
}

function listSSHDetails () {

    ls -l ./ssh_connections/*
}

function deleteSSHConnection () {
    ALIAS=$1
    
   rm ./ssh_connections/${ALIAS}.sh
}

function updateSSH () {
ALIAS=$1
    HOSTNAME=$2
    USERNAME=$3
    PORT=${4:-\2\2}
    KEY_PATH=${5:-~/.ssh/id_rsa.pub}
    SSH_CONNECTION_STRING="ssh -p $PORT -i $KEY_PATH $USERNAME@$HOSTNAME"

    serverDetails "$ALIAS" "$SSH_CONNECTION_STRING"  
}
