​
# UserManager Utility
​
This utility will perform the below operations:

**note :** as we are using this function to create group and user for system so we need sudo user for this so and we dont need to add sudo infront of script as we have changed /bin/bash​ to /bin/sudo so that script will run as sudo.
​
## Operations
​
| Operations        | Description                                                              |
| ----------------- | ------------------------------------------------------------------ |
| addTeam | To add a new group |
| addUser | To add a new user and adding it to a group with some more features |
​
## Steps to execute utility
​
1. To add a new group
​
```
./UserManager.sh addTeam [GroupName]
```
To check groups run below command:
```
cat /etc/group
```
​
2. To add a new user and then adding it to a group
```
./UserManager.sh addUser [UserName] [Groupname]
```
On successful execution of above command it will perform below actions:
- Add a new user with given name.
To check user run below command
```
cat /etc/passwd
```
- Add the user to the given group name.
- It will give user read,write & execute access to his home directory
- It will create two folders inside User's home directory having names ninja and second one with the groupname mentioned in the command.
- It will give all the users of same team read and excute access to home directory of fellow team members.
​
3. To check the help manual.
```
./UserManager.sh help
```

4. log file is created which will record all the entry which is created by using this function.
​
​
​

## Authors
​
- [@nikhil.panchal]