#!/bin/sudo bash
#set -x
source /home/nikhil/assignment/2/Userfunction.sh

addTeam()
{ 
    groupadd_function "$@" 
}

addUser() 
{   
    username=$1

    l=`getent passwd $username`
        x=`echo $?`
    if [[ $x == 0 ]]
        then
            echo " $username user already exist "
            echo " $date $user FAILED : $username user already exist " >> $logfile
        else
            user_add "$@"
            directory_add "$@"
            change_permission "$@"
            change_group "$@"
            change_owner "$@"
    fi
}

