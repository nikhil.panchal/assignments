#!/bin/sudo bash
#set -x 
date=`date`
user=`whoami`


groupadd_function()
{
    groupname=$1

    l=`getent group $groupname`
    x=`echo $?`

    if [ $x == 0 ]
    then
        echo " group already exist "
        echo " $date $user FAILED : $groupname group already exist " >> $logfile
    else
        groupadd $groupname
        echo " $groupname group added "
        echo " $date $user successful : $groupname group created " >> $logfile
    fi    
}



user_add()
{
    username=$1
    groupname=$2

    useradd -s /bin/bash -N -G $groupname,ninja $username
    passwd $username
                
    echo " $username added to $groupname group "
    echo " $username added to ninja group "
}

directory_add()
{
    username=$1
    groupname=$2

    mkdir -p /home/$username/ninja 
    echo " ninja directory created for $username "
    
    mkdir -p /home/$username/$groupname
    echo " $groupname directory created for $username "

}

change_permission()
{
    username=$1
    groupname=$2

    chmod 755 /home/$username
    chmod 770 /home/$username/ninja /home/$username/$groupname
}

change_group()
{   
    username=$1
    groupname=$2

    chgrp ninja /home/$username/ninja
    chgrp $groupname /home/$username/$groupname /home/$username
}

change_owner()
{   
    username=$1
    groupname=$2
    
    chown $username /home/$username /home/$username/ninja /home/$username/$groupname
}

user_help()
{
    echo " UserManager is used to add group and user.
    
    syntax :
    ./UserManager.sh [function] 

    functions:
    1) addTeam : 
    this function is used for creating desired group.

    ./UserManager.sh [addTeam] [group name to create]

    2) addUser
    this function is used for creating user and adding this user to already existing group at same time.

    ./UserManager.sh [addUser] [User name to create] [group name in which user need to add]

    THANK YOU
     " 
}