
​
# Assignment-8


```
Linux Day 8 Assignment
## Must Do
- Installation of webservers
- Please perform below mentioned acivities once the nginx/apache is installed
## Ip Based Virtual Hosting
    - Create a static website with basic html which should open using system IP
    - Create a static website with IP Based Virtual Hosting, should only open when hit using specific ip. For example it should open with 127.0.0.1 only
    - Create multiple static websites within the server having different web pages serving different content when hit using respective IP. For example, it should serve "Hi! Welcome to Ninja Program. You've reached localhost" when you hit on 127.0.0.1 where as it should serve "Hi! Welcome to Ninja Program. You've reached Pvt. IP" when you hit your server using private IP
## Name Based Virtual Hosting
    - Create a static website which should serve one page when hit using http://<your-name>.opstreeninja.com and a different page if hit using IP.
    - Should have nginx config which can serve "Hi! This is <your-name>, and I am a ninja" when you hit using http://<your-name>.opstreeninja.com and should serve "I am here to become a Devops Engineer" when hit using http://<your-name>.otninja.com.
## Nginx Path Based Routing
    - Create a website, it should serve "Welcome to my website" when hit using http://<your-name>.opstreeninja.com and should serve "This is <your-name>'s home page" when hit using http://<your-name>.opstreeninja.com/home
## Redirection http to http and http to https
    - Configure your nginx config in a way that if you hit http://<your-name>.opstreeninja.com, it should redirect you to http://<your-name>.redirect.opstreeninja.com
    - Configure your nginx config in a way that if you hit http://<your-name>.opstreeninja.com, it should redirect you to http://<your-name>.opstreeninja.com/home and should serve "Welcome to this website"
    - Configure your nginx in a way that you hit http://<your-name>.opstreeninja.com, it should return https://<your-name>.opstreeninja.com
```
​
##  Solution 

### task 1 : installation of nginx
to install nginx please use below command
````
$ apt update
$ apt-get install -y nginx
````
​here we are using -y for giving yes permission by default.
​

### Task 2: Ip Based Virtual Hosting
​
- Create a static website with basic html which should open using system IP
​
Step - 1
```
$ cd /etc/nginx/sites-available/
​
$ sudo vi nikhil
 
server {
	listen 80;
	
​
	server_name 13.127.78.240;
​
	root /var/www/html/nikhil/;
	index nikk.html;
​
	location / {
		try_files $uri $uri/ =404;
	}
}
```
Step 2 -
```
$ cd /etc/nginx/sites-enabled/
​
$ ln -s etc/nginx/sites-available/nikhil nikhil
```
step 3 -
```
$ cd /var/www/html/nikhil/
​
$ sudo vi nikk.html
```
​
​
- Create a static website with IP Based Virtual Hosting, should only open when hit using specific ip. For example it should open with 127.0.0.1 only
​
Step 1-
```
$ cd /etc/nginx/sites-available/
​
$ sudo vi local.nikk
 
server {
	listen 127.0.0.1:80;
	
​
	server_name local.nikk;
​
	root /var/www/html/locl.nikk/;
	index localnikk.html;
​
	location / {
		try_files $uri $uri/ =404;
	}
}
```
Step 2-
```
$ cd /etc/nginx/sites-enabled/
​
$ ln -s etc/nginx/sites-available/local.nikk local.nikk
​
```
Step 3-
```
$ sudo vi /etc/hosts
​
127.0.0.1 local.nikk
```
​
-  Create multiple static websites within the server having different web pages serving different content when hit using respective IP. For example, it should serve "Hi! Welcome to Ninja Program. You've reached localhost" when you hit on 127.0.0.1 where as it should serve "Hi! Welcome to Ninja Program. You've reached Pvt. IP" when you hit your server using private IP
​
Step 1
```
$ cd /etc/nginx/sites-available/
​
$ sudo vi local.ninja
​
server {
​
        listen :80;
        
​
        server_name 127.0.0.1;
​
        root /var/www/html/local.ninja/;
        index localninja.html;
​
        location / {
                try_files $uri $uri/ =404;
        }
}
```
Step2 
```
$ cd /etc/nginx/sites-enabled/
​
$ sudo ln -s /etc/nginx/sites-available/local.ninja local.ninja
```
Step 3
```
$ cd /var/www/html/local.ninja/
​
$ sudo vi localninja.html
​
Hi! Welcome to Ninja Program. You've reached localhost
```
```
$ curl 127.0.0.1
​
Hi! Welcome to Ninja Program. You've reached localhost
```
step 4 
```
$ cd /etc/nginx/sites-available/
​
$ sudo vi pvt.ninja
​
server {
        listen 80;
​
​
        server_name 172.31.41.3;
​
        root /var/www/html/pvt.ninja/;
        index pvtninja.html;
​
        location / {
                try_files $uri $uri/ =404;
        }
}
```
step 5
```
$ cd /etc/nginx/sites-enabled/
​
$ sudo ln -s /etc/nginx/sites-available/pvt.ninja pvt.ninja
```
step 6
```
$ cd /var/www/html/pvt.ninja/
​
$sudo vi pvtninja.html
​
Hi! Welcome to Ninja Program. You've reached Pvt. IP
```
```
$ curl 172.31.41.3
​
Hi! Welcome to Ninja Program. You've reached Pvt. IP
```
### Task 3 : Name Based Virtual Hosting 
​
- Create a static website which should serve one page when hit using http://<your-name>.opstreeninja.com and a different page if hit using IP.
​
Step 1
```
$ cd /etc/nginx/sites-available/
​
$ sudo vi nikhil.opstreeninja
​
server {
        listen 80;
        
​
        server_name nikhil.opstreeninja.com;
​
        root /var/www/html/nikkops/;
        index nikhil.html;
​
        location / {
                try_files $uri $uri/ =404;
        }
}
​
$ cd /etc/nginx/sites-enabled/
​
$ sudo ln -s /etc/nginx/sites-available/nikhil.opstreeninja nikkops
```
Step 2 
```
$ cd /var/www/html/nikkops/
​
$ sudo vi nikkops.html
​
Hello from nikhil. This is nikhil.opstreeninja.com
```
Step 3
```
$ cd /etc/nginx/sites-available/
​
$ sudo vi serverIP
 
server {
	listen 80;
	
​
	server_name 13.127.78.240;
​
	root /var/www/html/serverIP/;
	index serverIP.html;
​
	location / {
		try_files $uri $uri/ =404;
	}
}
​
$ cd /etc/nginx/sites-enabled/
​
$ ln -s etc/nginx/sites-available/server IP serverIP
​
$ cd /var/www/html/rserverIP/
​
$ sudo vi serverIP.html
​
Hello this is server IP 
```
​
step 4
```
$ vi /etc/hosts
​
13.127.78.240 nikhil.opstreeninja.com
​
$ curl nikhil.opstreeninja.com
​
Hello from nikhil. This is nikhil.opstreeninja.com
```
```
$ curl nikhil.opstreeninja.com
​
Hello from nikhil. This is nikhil.opstreeninja.com
​
​
$ curl 13.127.78.240
​
Hello this is server IP
```
​
- Should have nginx config which can serve "Hi! This is <your-name>, and I am a ninja" when you hit using http://<your-name>.opstreeninja.com and should serve "I am here to become a Devops Engineer" when hit using http://<your-name>.otninja.com.
​
Step 1
```
$ cd /etc/nginx/sites-available/
​
$ sudo vi nikhil.opstreeninja
​
server {
        listen 80;
        
​
        server_name nikhil.opstreeninja.com;
​
        root /var/www/html/nikkops/;
        index nikkops.html;
​
        location / {
                try_files $uri $uri/ =404;
        }
}
​
$ cd /etc/nginx/sites-enabled/
​
$ sudo ln -s /etc/nginx/sites-available/nikhil.opstreeninja opstreeninja
```
Step 2 
```
$ cd /var/www/html/nikkops/
​
$ sudo vi nikkops.html
​
Hi! This is nikhil Panchal, and I am a ninja
```
Step 3
```
$ cd /etc/nginx/sites-available/
$ sudo vi nikhil.otninja
​
server {
        listen 80;
​
​
        server_name nikhil.otninja.com;
​
        root /var/www/html/nikkotninja/;
        index nikkotninja.html;
​
        location / {
                try_files $uri $uri/ =404;
        }
}
​
$ cd /etc/nginx/sites-enabled/
​
$ sudo ln -s /etc/nginx/sites-available/nikhil.otninja nikhil.otninja
```
Step 4 
```
$ vi /etc/hosts
​
13.127.78.240 nikhil.opstreeninja.com
13.127.78.240 nikhil.otninja.com
```
```
$ curl nikhil.opstreeninja.com
Hi! This is nikhil Panchal, and I am a ninja
​
$ curl nikhil.otninja.com
I am here to become a Devops Engineer
```
### Task 4 : Nginx Path Based Routing
​
- Create a website, it should serve "Welcome to my website" when hit using http://<your-name>.opstreeninja.com and should serve "This is <your-name>'s home page" when hit using http://<your-name>.opstreeninja.com/home
​
Step 1
```
server {
        listen 80;
        
​
        server_name nikhil.opstreeninja.com;
​
        root /var/www/html/nikkops/;
        index nikkops.html;
        
        location /home {
        root /var/www/html/nikkops/;
        index nikkhome.html;        
        }
        location / {
                try_files $uri $uri/ =404;
        }
}
```


# Author

[@nikhil.panchal]