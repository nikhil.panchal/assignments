____________________________________
# ASSIGNMENT 1 : GIT
____________________________________

## 1. Create a folder ninja at the root level of your cloned code

**Step - 1**

create folder/directory named ninja at root level
.md
```
$ mkdir ninja
```

**step - 2**

after creating directory use clone command to clone gitlab directory by adding clone link

```
$ git clone <Gitlab clone link>
```
this will create linked repository in the current working directory.

>example is give below.

![](/home/nikhil/pictures/git_clone.png)



## 2. Add a file README.md with content "Trying fast forward merge"

so after cloning repository all the content will be available in directory.

now open text editor and open file.
```
$ vim README.md
```
add text "Trying fast forward merge" into the README.md file.

## 3.  Create a branch ninja and move to it
**Step - 1**
to create branch we will use below command
```
$ git branch ninja
```
this will create ninja branch in local repository

**step - 2**
to change branch we use checkout command first lets see in which branch we are wornking so by using below command we can chech our working branch
```
$ git branch
```

## 4. Run git status command
## 5. Commit your changes to ninja branch
## 6. Merge ninja branch to master branch make sure that a new commit get's created
## 7. Assuming you are in master branch, modify README.md with content Changes in master branch, commit the changes in master branch.
## 8. Switch to ninja branch, modify README.md with content Changes in ninja branch, commit the changes in ninja branch.
## 9. Merge master branch to ninja branch in such a fashion that changes of master branch overrides changes in ninja branch
## 10. Revert the above merge commit
## 11. Merge master branch to ninja branch in such a fashion that changes of ninja branch overrides changes in master branch
## 12. Revert the above merge commit
## 13. Merge master branch to ninja branch in such a fashion that changes of both branches gets accumulated.