#!/bin/bash
#set -x
source function_kill.sh
#- Stop processes older then n days/hours
# find all Running Processes
# filter them by the given number of days or hour

while getopts ":d:h:m:" opt; 
do
    case $opt in
        d) kill_process_by_day ${OPTARG};;
        h) Kill_process_by_hour ${OPTARG};;
        m) kill_process_by_minute ${OPTARG};;
        *) exit
    esac
done


#d) kill_process_by_day ${OPTARG}; echo "hie00";;
#h) Kill_process_by_hour ${OPTARG}; echo "hello";;
#m) kill_process_by_minute ${OPTARG};;