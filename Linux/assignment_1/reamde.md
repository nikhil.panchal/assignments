# assignment-1
# FileManager Utility
​
This Utility will help you to do following operations:
​
​
​
 Operations         | Descriptions                                                                |
| ----------------- | ------------------------------------------------------------------ |
| addDir | To create a new directory |
| listFiles | To list all the files in a particular folder  |
| listDirs | To list all the directories |
| listAll | To list all the files and directories |
| deleteDir | To delete a directory |
| addFile | To create a new file |
| addContentToFile | Add content to a file |
| addContentToFileBegining | Add content at the begining of the file |
| showFileBeginingContent | Show top n lines of a file |
| showFileEndContent | Show last n lines of a file |
| showFileContentAtLine | Show contents of a specific line number |
| showFileContentForLineRange | Show conteint of a specfific line number range |
| moveFile | Move file from one location to another |
| copyFile | Copy file from one location to another |
| clearFileContent | To Clear file content |
| deleteFile | To delete a file |
| help | To open help manual |
​
​
# Steps to execute the above operations
​
​
1. To create a new directory
```
 ./FileManager.sh addDir [PathName] [DirectoryName]
```
​
2. To list all the files in a particular folder
```
 ./FileManager.sh listFiles [PathName]
```

3. To list all the directories
```
 ./FileManager.sh listDirs [PathName] 
```

4. To list all the files and directories
```
 ./FileManager.sh listAll [PathName]
```

5. To delete a directory
```
./FileManager.sh deleteDir [PathName] [DirectoryName]
```
​
6. To create a new file
```
 ./FileManager.sh addFile [PathName] [FileName]
```

7. Add content to a file
```
 ./FileManager.sh addContentToFile [PathName] [FileName] ["Content"]
```
​
8. Add content at the begining of the file
```
  ./FileManager.sh addContentToFileBegining [PathName] [FileName] ["Content"]
```
​
9. Show top n lines of a file
```
 ./FileManager.sh showFileBeginingContent [PathName] [FileName] [LineNumber]
```

10. Show last n lines of a file
```
 ./FileManager.sh showFileEndContent [PathName] [FileName] [LineNumber]
```

11. Show contents of a specific line number
```
 ./FileManager.sh showFileContentAtLine [PathName] [FileName] [LineNumber]
```

12. Show conteint of a specfific line number range
```
 ./FileManager.sh showFileContentForLineRange [PathName] [FileName] [LineNumber1] [LineNumber2]
```
​
13. Move file from one location to another
```
 ./FileManager.sh moveFile [Source] [Destination]
```

14. Copy file from one location to another
```
./FileManager.sh copyFile [Source] [Destination]
```
​
15. To Clear file content
```
 ./FileManager.sh clearFileContent [PathName] [FileName]
```
​
16. To delete a file
```
 ./FileManager.sh deleteFile [PathName] [FileName]
```
​
17. To open help manual
```
./FileManager.sh help
```

also all the fuction used by user is getiing recorded in log file.
to view log file use this command

```
cat logs.txt
```


thank you

