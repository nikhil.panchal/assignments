#!/bin/bash
#set -x

source functions.sh



case $function
        in
        addDir) addDir;;
        listFiles) listFiles;;
        listDirs) listDirs;;
        listAll) listAll;;
        deleteDir) deleteDir;;
        addFile) addFile;;
        addContentToFile) addContentToFile;;
        addContentToFileBegining) addContentToFileBegining;;
        showFileBeginingContent) showFileBeginingContent;;
        showFileEndContent) showFileEndContent;;
        showFileContentAtLine) showFileContentAtLine;;
        showFileContentForLineRange) showFileContentForLineRange;;
        moveFile) moveFile;;
        copyFile) copyFile;;
        clearFileContent) clearFileContent;;
        deleteFile) deleteFile;;
        --help) help;;
        *) echo " ./FileManger.sh : invalid function -- '$function' 
                Try './FileManager --help' for more information. "
esac
      

