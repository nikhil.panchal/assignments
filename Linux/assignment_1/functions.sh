#!/bin/bash
#set -x

function=$1
path=$2
file=$2
destination=$3
name=$3
content=$4
noofline=$4
range1=$4
range2=$5
whichline=$4
user=`whoami`
date=`date`
logfile=logs.txt
red=$(tput setaf 1)
green=$(tput setaf 2)




addDir()
{
	if [ -d "$path" ]
		then
			if [ -d "$path/$name" ]
				then echo " the directory already exist"
					echo " do you want to rewrite it ? (y/n)"
					read x
					case $x in
						y) mkdir -p $path/$name; echo " $date $user $function Successfully : Directory created : $name " >> $logfile ;;
						n) echo "thank you action is terminated"; echo " $date $user $function FAILED : action terminated " >> $logile;;
						*) echo " $date $user $function FAILED : wrong input " >> $logfile; exit
					esac
				else
					mkdir -p $path/$name
					echo " $date $user $function Successfully : Directory created : $name " >> $logfile
			fi
		else
			echo " please check path of the directory '$path'"
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
}                                                                                                                                                                    

listFiles()
{
	if [ -d "$path" ]
		then 
				ls -la $path |grep ^-
				echo " $date $user successfully listed all file of this directorty $path " >> $logfile
		else 
				echo " directory no exist please check '$path' "
				echo " $date $user $function  FAILED : wrong input " >> $logfile
			
	fi
}                                                                                                                                                                       
listDirs()
{
	if [ -d "$path" ]
		then 
			ls -la $path |grep ^d
			echo " $date $user $function successfully listed all directory of $path/$name " >> $logfile
		else 
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
}
listAll()
{
	if [ -d "$path/$name" ]
		then 	
			ls -la $path
			echo " $date $user $function successfully listed all of $path/$name " >> $logfile
		else 	
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
	
}
deleteDir()
{
	if [ -d "$path/$name" ] 
			then 
					x=`ls -A $path/$name`
						if [ "$x" ] 
							then
								echo "directory is not empty do you want to delete is (y/n)? "
								read x
								case $x in
										y) rm -r $path/$name; echo "directory deleted"; echo " $date $user $function successfully $path/$name deleted ]" >> $logfile;;
										n) echo " action is terminated ";echo " $date $user $function FAILED : action terminated " >> $logfile;;
										*) echo " $date $user $functio FAILED : wrong input " >> $logfile; exit
								esac
							else 
								rmdir $path/$name; echo " directory deleted "
								echo " $date $user $function successfully $path/$name deleted " >> $logfile
						fi
			else
				echo " directory does not exist please check "
				echo " $date $user $function FAILED : wrong input " >> $logfile
	fi			
}

addFile()
{
	if [ -d "$path" ] 
		then
			touch $path/$name
			echo " $date $user $function successfully $path/$name added" >> $logfile
		else 
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
}
addContentToFile()
{
	if [ -d "$path" ]
		then
			echo $content >> $path/$name
			echo " $date $user $function successfully added content to $path/$name " >> $logfile
		else
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
}
addContentToFileBegining()
{
	if [ -d "$path" ] 
		then
			x=`echo "$content";cat $path/$name`
			echo "$x" > $path/$name
			echo " $date $user $function successfully content added to begining of $path/$name " >> $logfile
		else 
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
}
showFileBeginingContent()
{
	if [ -d "$path" ] 
		then
			head -$noofline $path/$name
			echo " $date $user $function successfully showed starting content of $noofline line " >> $logfile
		else 
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
}
showFileEndContent()
{
	if [ -d "$path" ] 
		then
			tail -$noofline $path/$name
			echo " $date $user $function successfully showed content of last $noofline line " >> $logfile
		else 
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
}
showFileContentForLineRange()
{
	if [ -d "$path" ] 
		then
			head -$range2 $path/$name | tail +$range1
			echo " $date $user $function successfully showed content of $range1 to $range2 lines " >> $logfile
		else 
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input ]" >> $logfile
	fi
}
showFileContentAtLine()
{ 
	if [ -d "$path" ] 
		then
			head -$whichline $path/$name | tail -1
			echo " $date $user $function successfully showed content at line $whichline " >> $logfile
		else 
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
}

moveFile()
{
	if [ -d "$path" ] 
		then
			mv $file $destination
			echo " $date $user $function successfully moved $file to $destination " >> $logfile
		else 
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
}
copyFile()
{
	if [ -d "$path" ] 
		then
			cp $file $destination
			echo " $date $user $function successfully copied $file to $destination " >> $logfile
		else 
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
}
clearFileContent()
{
	if [ -d "$path" ] 
		then
			echo > $path/$name
			echo " $date $user $function successfully content of $path/$name cleared " >> $logfile
		else 
			echo " directory no exist please check '$path' "
			echo " $date $user $function  FAILED : wrong input " >> $logfile
	fi
}
deleteFile()
{
	if [ -d "$path" ] 
		then
			rm $path/$name
			echo " $date $user $function successfully $path/$name Deleted " >> $logfile
		else 
			echo " directory no exist please check '$path' "
			echo " $date $user $function FAILED : wrong input " >> $logfile
	fi
}                                                                                                                                                                          

help()
{
	echo " ./FileManager.sh this function is use to complete various type of task.
			usage:
			./FileManager.sh [function]

			use of function :

			[addDir] : to add directory at define path
			use : ./FileManager.sh [addDir] [path] [name]

        	[listFiles] : list only files in directory
			use : ./FileManager.sh [addDir] [path]

        	[listDirs] : list only directory in directory
			use : ./FileManager.sh [addDir] [path]

        	[listAll] : List all files in directory
			use : ./FileManager.sh [addDir] [path]

        	[deleteDir] : to delete directory
			use : ./FileManager.sh [addDir] [path]

        	[addFile] : to add file in directory
			use : ./FileManager.sh [addDir] [path] [name]

        	[addContentToFile] : to add content in file
			use : ./FileManager.sh [addDir] [path] [name] [content]

        	[addContentToFileBegining] : to add content in first line of file
			use : ./FileManager.sh [addDir] [path] [name] [content]

        	[showFileBeginingContent] : to see n number of like from top.
			use : ./FileManager.sh [addDir] [path] [name] [no of line]

        	[showFileEndContent] : to see n number of like from bottom.
			use : ./FileManager.sh [addDir] [path] [name] [no of line]

        	[showFileContentAtLine] : to see signle line of given number
			use : ./FileManager.sh [addDir] [path] [name] [no of line]

        	[showFileContentForLineRange] : to see line of file in range
			use : ./FileManager.sh [addDir] [path] [name] [range1] [range2]

        	[moveFile] : to move file from one directory to other directory, or to reaname file
			use : ./FileManager.sh [addDir] [name] [path]

        	[copyFile] : to copy file from one directory to other directory
			use : ./FileManager.sh [addDir] [name] [path]

        	[clearFileContent]. : to clear content of file.
			use : ./FileManager.sh [addDir] [path] [name]

        	[deleteFile] : to delete file
			use : ./FileManager.sh [addDir] [path] [name]

			thank you."
		echo "$green $date $user $function seen help " >> $logfile
}
