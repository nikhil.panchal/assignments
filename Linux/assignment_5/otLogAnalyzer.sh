#!/bin/bash
#set -x
#Today Assignment 5 :Create a a log processing utility, to find
#  - The list of all the IP's along with count
#   - Response Type: 200/204/301/304/503...
#   - Response Type in category: 2xx,3xx,4xx,5xx
#   - Category of responses by sizes: <100, <200, <500, <1000


source function.sh

function=$1
logg=$2

case $function 
in
    ip) Ip_Along_With_Count $logg ;;
    #rst) Response_Type
    #rsc) Response_Type_in_category
    #size) Category_responses_by_sizes
    --Help) Help;;
    *) echo " invalid input "
        echo " for more information use --Help "
esac


