#!/bin/bash
#set -x
function User_Help()
{
    echo " otlogAnalyzer.sh is used to view logs and filte logs of programm.
    
    syntax :
    ./otlogAnalyzer.sh [function] 

    functions:
    1) addTeam : 
    this function is used for creating desired group.

    ./UserManager.sh [addTeam] [group name to create]

    2) addUser
    this function is used for creating user and adding this user to already existing group at same time.

    ./UserManager.sh [addUser] [User name to create] [group name in which user need to add]

    THANK YOU
    " 
}

The_list_of_all_the_IP_along_with_count()
{
echo " IPs with count is below "
awk '{print $1}' $logg | sort | uniq -c
}

Response_Type(){
    echo " IPs with count is below "
awk -F " '{print $3}' $logg | sort | uniq -c
}
#Response_Type_in_category(){}
#Category_of_responses_by_sizes(){}