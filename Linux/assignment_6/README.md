# Assignment-6
# Text editor Utility and sed & grep Operations
​
This Utility will be able to do below operations:
​
| Utility             | Description                                                               |
| ----------------- | ------------------------------------------------------------------ |
| Task -1 : TempleEngine |
| Template Engine | this script is used for adding name in file |
| Task -2 : TextEditor |
| addLineTop       |  Add a line at top  |
| addLineBottom    |  Add a line at bottom |
|  addLineAt       | Add a line at specific line number |
|  updateFirstWord |   Replace word |
| updateAllWords   |  Delete word |
|  insertWord      |  Insert word |
| deleteLine       |   Delete a line |
|  deleteWord      |   Delete a line containeing a word |
| templateEngine | Create a template engine, that can generate values file and replace the variables provided as arguments |
​
​
## Task-1 : TemplateEngine

sed & grep
- Create a template engine, that can generate values file and replace the variables provided as arguments
./templateEngine.sh trainer.template key1=value1 key2=value2....

```
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ ./templateEngine.sh trainer.template nikhil maths
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ cat trainer.template 
mikhil is good trainer of maths
rakesh is good trainer of swimming
ashish is good trainer of coding
bhupesh is good trainer of cricket
vishal is good trainer of football
nikhil is trainer of maths
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ 

```

## Task-2 : otTextEditor

- Create a text editor utility, using which you can
    - Add a line at top
    - Add a line at bottom
    - Add a line at specific line number
    - Replace word
    - Delete word
    - Insert word
    - Delete a line
    - Delete a line containeing a word


**case 1**
./otTextEditor addLineTop <file> <line>
```
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ cat trainer.template 
mikhil is good trainer of maths
rakesh is good trainer of swimming
ashish is good trainer of coding
bhupesh is good trainer of cricket
vishal is good trainer of football
nikhil is trainer of maths

nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ ./otTextEditor.sh addLineTop trainer.template "hello i am good"

nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ cat trainer.template 
hello i am good
mikhil is good trainer of maths
rakesh is good trainer of swimming
ashish is good trainer of coding
bhupesh is good trainer of cricket
vishal is good trainer of football
nikhil is trainer of maths
```

**case 2**
./otTextEditor addLineBottom <file> <line>
````
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ cat trainer.template 
hello i am good
hello i am good
mikhil is good trainer of maths
rakesh is good trainer of swimming
ashish is good trainer of coding
bhupesh is good trainer of cricket
vishal is good trainer of football
nikhil is trainer of maths
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ ./otTextEditor.sh addLineBottom trainer.template "this is last line"
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ cat trainer.template 
hello i am good
hello i am good
mikhil is good trainer of maths
rakesh is good trainer of swimming
ashish is good trainer of coding
bhupesh is good trainer of cricket
vishal is good trainer of football
nikhil is trainer of maths
this is last line
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ 

````
**case 3**
./otTextEditor addLineAt <file> <linenumber> <line>

````
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ cat trainer.template 
hello i am good
hello i am good
mikhil is good trainer of maths
rakesh is good trainer of swimming
ashish is good trainer of coding
bhupesh is good trainer of cricket
vishal is good trainer of football
nikhil is trainer of maths
this is last line
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ ./otTextEditor.sh addLineAt trainer.template 5 "this is fifth line"
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ cat trainer.template 
hello i am good
hello i am good
mikhil is good trainer of maths
rakesh is good trainer of swimming
this is fifth line
ashish is good trainer of coding
bhupesh is good trainer of cricket
vishal is good trainer of football
nikhil is trainer of maths
this is last line
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ 

````

**case 4**
./otTextEditor updateFirstWord <file> <word>

````
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ cat trainer.template 
hello i am good
hello i am good
mikhil is good trainer of maths
rakesh is good trainer of swimming
this is fifth line
ashish is good trainer of coding
bhupesh is good trainer of cricket
vishal is good trainer of football
nikhil is trainer of maths
this is last line
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ ^C
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ ./otTextEditor.sh updateFirstWord trainer.template hie
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ cat trainer.template 
hie i am good
hie i am good
hie is good trainer of maths
hie is good trainer of swimming
hie is fifth line
hie is good trainer of coding
hie is good trainer of cricket
hie is good trainer of football
hie is trainer of maths
hie is last line
nikhil@nikhil-HP-Laptop-15-da0xxx:~/devops/Linux/assignment_6$ 
````

**note**
there are other function which use case can be seen by help function
​
## Authors
- [@nikhil.panchal]