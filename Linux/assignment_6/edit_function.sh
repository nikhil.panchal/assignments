#!/bin/bash
#set -x

#--help) user_help;;
function user_help()
{   echo 
" 
otTextEditor.sh is function use for editing file.

syntax :

    ./otTextEditor.sh [function] [file]

ALL THE FUNCTION AND THEIR USAGE GIVEN BELOW. 

1)addLineTop : this function is used to add line at top of the file.

syntax : 
./otTextEditor addLineTop <file> <line> 

2)addLineBottom : this function is used to add line at bottom of the file.

syntax : 
./otTextEditor addLineBottom <file> <line>

3)addLineAt : this function is used to add line at giver number line  in the file.

syntax : 
./otTextEditor addLineAt <file> <linenumber> <line>

4)updateFirstWord : this function will update the first word it find to the word provided.

syntax : 
./otTextEditor updateFirstWord <file> <word>

5)updateAllWords : it will change all the metined word to given word

syntax : 
./otTextEditor updateAllWords <file> <word>

6)insertWord : this will insert word after the metioned word.

syntax : 
./otTextEditor insertWord <file> <word1> <word2> <word to be inserted>

7)deleteLine : this function is used to delete given no of line in the file 

syntax : 
./otTextEditor deleteLine <file> <line no>

8)deleteWord) this function is used for deleting line which contain given word.

syntax : 
./otTextEditor deleteWord <file> <line no> <word>


THANK YOU FOR READING.

"
}

#addLineTop) add_top_line "$filename" "$content_of_line" ;;
function add_top_line()
{   
    filename=$1
    content_of_line="$2"
    
    if [ -f "$filename" ];then

    sed -i -e '1i \'"$content_of_line"'\' $filename
    
    else
    echo "file not exist please check filename"
    
    fi
}

#addLineBottom) add_bottom_line "$filename" "$content_of_line";;
function add_bottom_line()
{
    filename=$1
    content_of_line="$2"
    
    if [ -f "$filename" ];then

    sed -i -e '$ a '"$content_of_line"'\' $filename
    
    else
    echo "file not exist please check filename"
    
    fi
}

#addLineAt) add_line_at_Number "$filename" "$line_number" "$content_of_line";;
function add_line_at_Number()
{
    filename=$1
    line_number=$2
    content_of_line=$3
    
    if [ -f "$filename" ];then

    sed -i -e ''$line_number'i '"$content_of_line"'\' $filename
    
    else
    echo "file not exist please check filename"
    
    fi
}

#updateFirstWord) update_first_word "$filename" "$word";;
function update_first_word()
{
    filename=$1
    word=$2
    word2=$3
    
    if [ -f "$filename" ];then

    sed -i -e 's/^*/'$word'/1' $filename  
    
    else
    echo "file not exist please check filename"
    
    fi
}

#updateAllWords) update1_all_word  "$filename" "$word" "$word2";;
function update1_all_word()
{
    filename=$1
    old_word=$2
    new_word=$3
    
    if [ -f "$filename" ];then
    
    sed -i -e 's/'$old_word'/'$new_word'/g' $filename
    
    else
    echo "file not exist please check filename"
    
    fi
}


#insertWord) insert_word  "$filename" "$word" "$word2" "$word_to_insert";;
insert_word()
{
    filename=$1
    old_word1=$2
    old_word2=$3
    word_to_insert=$4
    
    if [ -f "$filename" ];then

    sed -i -e 's/'$old_word1' '$old_word2'/'$old_word1' '$word_to_insert' '$old_word2'/' $filename
    
    else
    echo "file not exist please check filename"
    
    fi
}

#deleteLine) delete_line  "$filename" "$line_number";;
function delete_line()
{
    filename=$1
    line_number=$2
    
    if [ -f "$filename" ];then
    
    sed -i -e ''$line_number'd' $filename
    else
    echo "file not exist please check filename"
    
    fi
}

#deleteWord) delete_word  "$filename" "$line_number" "$word2"
delete_word()
{
    filename=$1
    line_number=$2
    word=$3
    
    if [ -f "$filename" ];then

    sed -i -e '$line_number'i 's/'$word'/' '/' $filename
    
    else
    echo "file not exist please check filename"
    
    fi
}
