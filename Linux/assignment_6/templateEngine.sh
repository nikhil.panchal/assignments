#!/bin/bash
#set -x


#sed & grep
#- Create a template engine, that can generate values file and replace the variables provided as arguments
#./templateEngine.sh trainer.template key1=value1 key2=value2....
#```
#trainer.template
#fname is trainer of topic
#./templateEngine.sh <template file> fname=sandeep topic=linux
#sandeep is trainer of linux 

filename=$1
trainer_name=$2
trainer_topic=$3

if [ -f "$filename" ];then

    sed -i -e '$ a '$trainer_name' is trainer of '$trainer_topic'\' $filename

    else
    echo "file not exist please check filename"
fi
