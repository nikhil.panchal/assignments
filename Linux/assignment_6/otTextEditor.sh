#!/bin/bash
#set -x

#Create a text editor utility, using which you can
#1)Add a line at top
#2)Add a line at bottom
#3)Add a line at specific line number
#4)Replace word
#5)Delete word
#6)Insert word
#7)Delete a line
#8)Delete a line containeing a word

#./otTextEditor addLineTop <file> <line>
#./otTextEditor addLineBottom <file> <line>
#./otTextEditor addLineAt <file> <linenumber> <line>
#./otTextEditor updateFirstWord <file> <word>
#./otTextEditor updateAllWords <file> <word1> <word2>
#./otTextEditor insertWord <file> <word1> <word2> <word to be inserted>
#./otTextEditor deleteLine <file> <line no>
#./otTextEditor deleteWord <file> <line no> <word>

function=$1
filename=$2
line_number=$3
word=$3
word2=$4
word_to_insert=$5
content_of_line=$3



source edit_function.sh

case $function
    in 
        addLineTop) add_top_line "$filename" "$content_of_line" ;;
        addLineBottom) add_bottom_line "$filename" "$content_of_line";;
        addLineAt) add_line_at_Number "$filename" "$line_number" "$word2";;
        updateFirstWord) update_first_word "$filename" "$word";;
        updateAllWords) update1_all_word  "$filename" "$word" "$word2";;
        insertWord) insert_word  "$filename" "$word" "$word2" "$word_to_insert";;
        deleteLine) delete_line  "$filename" "$line_number";;
        deleteWord) delete_word  "$filename" "$line_number" "$word2";;
        --help) user_help;;
        *) echo " wrong input : for more information type --help with function "

esac


